package settings

import (
	"github.com/gotk3/gotk3/glib"
	"github.com/pkg/errors"
	"gitlab.com/jtvi/podcast/app/logging"
	"gitlab.com/jtvi/podcast/app/signals"
)

const APPID = "com.gitlab.jtvi.podcast_app"

const schemaId = "com.gitlab.jtvi.Podcast"

var settings *glib.Settings

func init() {
	if settings == nil {
		settings = glib.SettingsNew(schemaId)
	}
}

func GetBoolean(name string) bool {
	checkInit()
	return settings.GetBoolean(name)
}

func GetFloat(name string) float64 {
	checkInit()
	return settings.GetDouble(name)
}
func GetString(name string) string {
	checkInit()
	return settings.GetString(name)
}

func GetInt(name string) int {
	checkInit()
	return settings.GetInt(name)
}

func SetBoolean(name string, value bool) {
	checkInit()
	settings.SetBoolean(name, value)
	emitEvent(name, value)
}
func SetFloat(name string, value float64) {
	checkInit()
	settings.SetDouble(name, value)
	emitEvent(name, value)

}
func SetString(name string, value string) {
	checkInit()
	settings.SetString(name, value)
	emitEvent(name, value)
}
func SetInt(name string, value int) {
	settings.SetInt(name, value)
	emitEvent(name, value)
}
func emitEvent(name string, value interface{}) {
	signals.EmitSettingsChanged(&name, value)
}

func checkInit() {
	if settings == nil {
		err := errors.New("Settings must be initialized before use")
		logging.LogCritical(err)
	}
}
