package utils

import (
	"io/ioutil"
	"net/http"
)

//ExecuteGetRequest executes a GET-request from a  given url.
func ExecuteGetRequest(urlString string) ([]byte, error) {
	resp, err := http.Get(urlString)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	err = resp.Body.Close()
	if err != nil {
		return nil, err
	}
	return body, nil
}
