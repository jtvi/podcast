package utils

import (
	"github.com/nu7hatch/gouuid"
	"gitlab.com/jtvi/podcast/app/logging"
)

func NewGuidString() string {
	guid, err := uuid.NewV4()
	logging.LogCritical(err)
	return guid.String()
}
