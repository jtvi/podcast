package common

type ViewType string

const (
	ViewTypeSearchResult      ViewType = "ViewTypeSearchResult"
	ViewTypeEmptySearchResult ViewType = "ViewTypeEmptySearchResult"
	ViewTypeTop100Result      ViewType = "ViewTypeTop100Result"
	ViewTypePodcastView       ViewType = "ViewTypePodcastView"
)
