package ui

import (
	"gitlab.com/jtvi/podcast/app/dataTypes"
	"gitlab.com/jtvi/podcast/app/signals"
)

type ViewHistory struct {
	views       []dataTypes.ViewInterface
	currentView dataTypes.ViewInterface
}

func NewViewHistory() *ViewHistory {
	viewHistory := new(ViewHistory)
	views := make([]dataTypes.ViewInterface, 0)
	viewHistory.views = views
	return viewHistory
}

func (viewHistory *ViewHistory) Insert(view dataTypes.ViewInterface, currentView dataTypes.ViewInterface) {
	currentViewIndex := viewHistory.getIndex(currentView)

	if currentViewIndex == viewHistory.len()-1 {
		viewHistory.views = append(viewHistory.views, view)

		return
	}

	if currentViewIndex == 0 && viewHistory.len() > 1 {
		currentViewIndex = 1
	}
	viewsNeeded := (viewHistory.views)[:currentViewIndex]
	viewsUnNeeded := (viewHistory.views)[currentViewIndex+1:]
	viewHistory.views = append(viewsNeeded, view)
	if len(viewsUnNeeded) > 0 {
		signals.EmitViewHistoryChanged(nil, viewsUnNeeded)
	}
}

func (viewHistory *ViewHistory) GetPrevious(currentView dataTypes.ViewInterface) (dataTypes.ViewInterface, bool) {
	currentIndex := viewHistory.getIndex(currentView)
	previousIndex := currentIndex - 1
	if !viewHistory.indexExists(previousIndex) {
		return nil, false
	}
	return viewHistory.index(previousIndex)
}

func (viewHistory *ViewHistory) GetNext(currentView dataTypes.ViewInterface) (dataTypes.ViewInterface, bool) {
	currentIndex := viewHistory.getIndex(currentView)
	nextIndex := currentIndex + 1
	if !viewHistory.indexExists(nextIndex) {
		return nil, false
	}

	return viewHistory.index(nextIndex)
}

func (viewHistory *ViewHistory) indexExists(index int) bool {
	if index < 0 || index > viewHistory.len()-1 {
		return false
	}
	return true
}

func (viewHistory *ViewHistory) index(index int) (dataTypes.ViewInterface, bool) {
	listLength := viewHistory.len()

	if !viewHistory.indexExists(index) {
		return nil, false
	}

	for i := 0; i < listLength; i++ {
		if i == index {
			return (viewHistory.views)[i], true
		}
	}
	return nil, false
}

func (viewHistory *ViewHistory) getIndex(view dataTypes.ViewInterface) int {
	for i := 0; i < viewHistory.len(); i++ {
		if (viewHistory.views)[i].GetID() == view.GetID() {
			return i
		}
	}
	return -1
}

func (viewHistory *ViewHistory) len() int {
	return len(viewHistory.views)
}
func (viewHistory *ViewHistory) GetNavigationStatus() *dataTypes.NavigationStatus {
	//currentIndex := viewHistory.getIndex(viewHistory.currentView)
	navigationStatus := new(dataTypes.NavigationStatus)

	//if currentIndex == 0 && vm.viewHistory.Len() > 1 {
	//	navigationStatus.CanNavigateBack = false
	//	navigationStatus.CanNavigateForward = true
	//} else if currentIndex == 0 && vm.viewHistory.Len() <= 1 {
	//	navigationStatus.CanNavigateBack = false
	//	navigationStatus.CanNavigateForward = false
	//} else if currentIndex == vm.viewHistory.Len()-1 {
	//	navigationStatus.CanNavigateBack = true
	//	navigationStatus.CanNavigateForward = false
	//} else {
	//	navigationStatus.CanNavigateBack = true
	//	navigationStatus.CanNavigateForward = true
	//}
	return navigationStatus
}
