package ui

import (
	"github.com/gotk3/gotk3/gdk"
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/jtvi/podcast/app/dataTypes"
	"gitlab.com/jtvi/podcast/app/logging"
	"gitlab.com/jtvi/podcast/app/ui/views"
)

type MainWindow struct {
	*gtk.ApplicationWindow
	viewStack     *gtk.Stack
	bottomToolbar *views.BottomToolbar
	controller    *MainWindowController
}

func NewMainWindow(gtkApplication *gtk.Application, controller *MainWindowController) MainWindow {
	mainWindow := MainWindow{}
	mainWindow.controller = controller
	appWindow, err := gtk.ApplicationWindowNew(gtkApplication)
	logging.LogCritical(err)
	appWindow.SetDefaultSize(900, 600)
	mainWindow.ApplicationWindow = appWindow

	root, err := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 5)

	logging.LogCritical(err)
	viewStack, err := gtk.StackNew()
	logging.LogCritical(err)
	viewStack.SetTransitionType(gtk.STACK_TRANSITION_TYPE_CROSSFADE)
	viewStack.SetHExpand(true)
	viewStack.SetVAlign(gtk.ALIGN_FILL)
	mainWindow.viewStack = viewStack

	topToolbar := views.TopToolbarNew(mainWindow.controller)

	mainWindow.controller.ConnectViewChanged(func(activeView dataTypes.ViewInterface, navigationStatus *dataTypes.NavigationStatus) {
		child := mainWindow.viewStack.GetChildByName(activeView.GetID())
		if child == nil {
			mainWindow.viewStack.AddNamed(activeView, activeView.GetID())
		}
		mainWindow.viewStack.SetVisibleChildName(activeView.GetID())
	})
	//
	//signals.ConnectViewHistoryChanged(nil, func(views []dataTypes.ViewInterface) {
	//	for _, view := range views {
	//		mainWindow.viewStack.Remove(view)
	//	}
	//})

	mainWindow.SetTitlebar(topToolbar)

	bottomToolbar := views.NewBottomToolbar()
	root.PackEnd(bottomToolbar, false, false, 3)
	root.Add(viewStack)
	mainWindow.Add(root)

	_, _ = mainWindow.Connect("button-release-event", func(window *gtk.ApplicationWindow, event *gdk.Event) bool {
		eventButton := gdk.EventButtonNewFromEvent(event)
		switch eventButton.Button() {
		case 8:
			controller.Navigate(dataTypes.DirectionPrevious)
			return true
		case 9:
			controller.Navigate(dataTypes.DirectionNext)
			return true
		}
		return false
	})

	mainWindow.controller.Start()
	return mainWindow
}
