package views

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/jtvi/podcast/app/logging"
	"gitlab.com/jtvi/podcast/app/signals"
)

type BottomToolbar struct {
	*gtk.Box
	pauseImage *gtk.Image
	playImage  *gtk.Image
}

func NewBottomToolbar() *BottomToolbar {
	btb := new(BottomToolbar)

	playImg, err := gtk.ImageNewFromIconName("media-playback-start-symbolic", gtk.ICON_SIZE_LARGE_TOOLBAR)
	logging.LogCritical(err)
	btb.playImage = playImg

	pauseImg, err := gtk.ImageNewFromIconName("media-playback-pause-symbolic", gtk.ICON_SIZE_LARGE_TOOLBAR)
	logging.LogCritical(err)
	btb.pauseImage = pauseImg

	bottomBox, err := gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, 0)
	logging.LogCritical(err)

	centerBox, err := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 0)
	logging.LogCritical(err)
	centerBox.SetSizeRequest(165, 10)

	playerControlsBox, err := gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, 0)
	logging.LogCritical(err)

	previousButton, err := gtk.ButtonNewFromIconName("media-skip-backward-symbolic", gtk.ICON_SIZE_LARGE_TOOLBAR)
	logging.LogCritical(err)

	_, _ = previousButton.Connect("button-press-event", func() bool {
		signals.EmitPlayPrevious(nil)
		return true
	})
	previousButton.SetVAlign(gtk.ALIGN_CENTER)
	playerControlsBox.PackStart(previousButton, true, false, 3)
	previousButton.SetSensitive(false)

	playButton, err := gtk.ButtonNew()
	logging.LogCritical(err)
	playButton.SetImage(btb.playImage)
	_, _ = playButton.Connect("button-press-event", func() bool {
		signals.EmitTogglePlay(nil)
		return true
	})

	playButton.SetVAlign(gtk.ALIGN_CENTER)
	playerControlsBox.PackStart(playButton, true, false, 3)
	playButton.SetSensitive(false)

	nextButton, err := gtk.ButtonNewFromIconName("media-skip-forward-symbolic", gtk.ICON_SIZE_LARGE_TOOLBAR)
	logging.LogCritical(err)
	_, _ = nextButton.Connect("button-press-event", func() bool {
		signals.EmitPlayNext(nil)
		return true
	})
	nextButton.SetVAlign(gtk.ALIGN_CENTER)
	playerControlsBox.PackStart(nextButton, true, false, 3)
	nextButton.SetSensitive(false)

	progressScale, err := gtk.ScaleNewWithRange(gtk.ORIENTATION_HORIZONTAL, 0, 1000000.0, 1)
	logging.LogCritical(err)
	progressScale.SetMarginStart(10)
	progressScale.SetMarginEnd(10)
	progressScale.SetDrawValue(false)
	_, _ = progressScale.Connect("change-value", func(sender interface{}, changeType int, value float64) bool {
		signals.EmitSeek(nil, value)
		return false
	})
	centerBox.PackStart(playerControlsBox, false, false, 0)
	centerBox.PackStart(progressScale, true, true, 3)

	bottomBox.SetCenterWidget(centerBox)

	volumeBox, err := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 5)
	logging.LogCritical(err)

	volumeButton, err := gtk.VolumeButtonNew()
	logging.LogCritical(err)
	_, _ = volumeButton.Connect("value-changed", func(volumeButton interface{}, value float64) {
		//		signals.EmitSetVolume(nil, value)
	})
	//signals.ConnectVolumeChanged(nil, func(value float64) {
	//	volumeButton.SetValue(value)
	//})
	//
	//signals.ConnectPlaybackPositionChanged(nil, func(position int64, duration int64) {
	//	progress := position * 1.0
	//	progressTime := time.Unix(0, position).UTC()
	//	durationTime := time.Unix(0, duration).UTC()
	//	durationFloat := float64(duration)
	//	progressScale.SetRange(0, durationFloat)
	//	progressScale.SetValue(float64(progress))
	//	progressScale.SetTooltipText(fmt.Sprintf("%s/%s", progressTime.Format("15:04:05"), durationTime.Format("15:04:05")))
	//})

	volumeBox.SetCenterWidget(volumeButton)
	bottomBox.PackEnd(volumeBox, false, false, 10)
	//
	//signals.ConnectPlaybackChanged(nil, func(status dataTypes.Status) {
	//	nextButton.SetSensitive(status.HasNext)
	//	previousButton.SetSensitive(status.HasPrevious)
	//
	//	playButton.SetSensitive(status.CanPlay)
	//	if status.IsPlaying {
	//		playButton.SetImage(btb.pauseImage)
	//	} else {
	//		playButton.SetImage(btb.playImage)
	//	}
	//})
	btb.Box = bottomBox
	return btb
}
