package views

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/jtvi/podcast/app/gtkAsyncLoader"
	"gitlab.com/jtvi/podcast/app/library/sourceManager/interfaces"
	"gitlab.com/jtvi/podcast/app/logging"
	common2 "gitlab.com/jtvi/podcast/app/ui/common"
	"gitlab.com/jtvi/podcast/app/ui/widgets"
)

type Top100View struct {
	*gtk.Box
	*gtkAsyncLoader.Controller
	controller     Top100ViewController
	podcastFlowbox *widgets.PodcastListView
	id             string
}

type Top100ViewController interface {
	GetRecommendations() ([]interfaces.PodcastMetaData, error)
	PodcastMetaSelected(podcastMetaData interfaces.PodcastMetaData)
}

func NewTop100View(controller Top100ViewController) *Top100View {
	top100View := new(Top100View)
	top100View.controller = controller

	box, err := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 0)
	logging.LogCritical(err)
	podcastFlowbox, err := widgets.NewPodcastFlowbox(controller)
	logging.LogCritical(err)

	top100View.podcastFlowbox = podcastFlowbox
	box.PackStart(podcastFlowbox, true, true, 0)

	top100View.Box = box

	podcasts, err := controller.GetRecommendations()
	if err != nil {
		// TODO: Handle error
	}

	top100View.podcastFlowbox.AddPodcasts(podcasts)
	top100View.podcastFlowbox.Show()
	top100View.ShowAll()
	return top100View
}

func (tv *Top100View) GetID() string {
	return tv.id
}

func (tv *Top100View) GetViewType() string {
	return string(common2.ViewTypeTop100Result)
}
