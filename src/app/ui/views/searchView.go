package views

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/jtvi/podcast/app/library/sourceManager/interfaces"
	"gitlab.com/jtvi/podcast/app/logging"
	common2 "gitlab.com/jtvi/podcast/app/ui/common"
	"gitlab.com/jtvi/podcast/app/ui/widgets"
	"gitlab.com/jtvi/podcast/app/utils"
)

type SearchView struct {
	*gtk.Box
	podcastFlowbox *widgets.PodcastListView
	id             string
	controller     SearchViewController
}

type SearchViewController interface {
	Search(searchTerm string) ([]interfaces.PodcastMetaData, error)
	PodcastMetaSelected(podcastMetaData interfaces.PodcastMetaData)
}

func NewSearchView(searchTerm string, controller SearchViewController) *SearchView {
	searchView := new(SearchView)
	searchView.controller = controller
	searchView.id = utils.NewGuidString()
	box, err := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 0)
	logging.LogCritical(err)
	podcastFlowbox, err := widgets.NewPodcastFlowbox(controller)
	logging.LogCritical(err)

	searchView.podcastFlowbox = podcastFlowbox

	podcasts, err := searchView.controller.Search(searchTerm)
	if err != nil {
		//TODO: handle error
	}
	searchView.podcastFlowbox.AddPodcasts(podcasts)
	box.PackStart(podcastFlowbox, true, true, 0)
	searchView.Box = box
	searchView.ShowAll()
	return searchView
}

func (searchView *SearchView) GetID() string {
	return searchView.id
}

func (searchView *SearchView) GetViewType() string {
	return string(common2.ViewTypeSearchResult)
}
