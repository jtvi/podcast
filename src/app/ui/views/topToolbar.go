package views

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/jtvi/podcast/app/dataTypes"
	"gitlab.com/jtvi/podcast/app/logging"
	"gitlab.com/jtvi/podcast/app/translations"
	"gitlab.com/jtvi/podcast/app/ui/common"
)

type TopToolbar struct {
	*gtk.HeaderBar
	dataAccess     TopToolbarDataInterface
	previousButton *gtk.Button
	nextButton     *gtk.Button
}

type TopToolbarDataInterface interface {
	RequestViewChange(viewType common.ViewType, params interface{})
	Navigate(direction dataTypes.NavigationDirection)
	ConnectViewChanged(callback func(activeView dataTypes.ViewInterface, navigationStatus *dataTypes.NavigationStatus))
}

func TopToolbarNew(dataInterface TopToolbarDataInterface) *TopToolbar {
	topToolbar := new(TopToolbar)
	topToolbar.dataAccess = dataInterface
	err := topToolbar.OnLoadUI()
	logging.LogCritical(err)

	return topToolbar
}

func (toolbar *TopToolbar) OnLoadUI() error {
	headerBar, err := gtk.HeaderBarNew()
	logging.LogCritical(err)

	headerBar.SetShowCloseButton(true)

	goPrevious, err := gtk.ButtonNewFromIconName("go-previous", gtk.ICON_SIZE_LARGE_TOOLBAR)
	logging.LogCritical(err)

	headerBar.PackStart(goPrevious)
	toolbar.previousButton = goPrevious
	_, _ = goPrevious.Connect("clicked", func() {
		toolbar.dataAccess.Navigate(dataTypes.DirectionPrevious)
	})

	goNext, err := gtk.ButtonNewFromIconName("go-next", gtk.ICON_SIZE_LARGE_TOOLBAR)
	logging.LogCritical(err)

	headerBar.PackStart(goNext)
	toolbar.nextButton = goNext
	_, _ = goNext.Connect("clicked", func() {
		toolbar.dataAccess.Navigate(dataTypes.DirectionNext)
	})

	storeButton, err := gtk.ButtonNewFromIconName("application-rss+xml-symbolic", gtk.ICON_SIZE_SMALL_TOOLBAR)
	logging.LogCritical(err)

	storeButton.SetAlwaysShowImage(true)
	storeButton.SetLabel(translations.T("iTunes Store"))

	context, err := storeButton.GetStyleContext()
	logging.LogCritical(err)

	context.AddClass("h2")

	_, _ = storeButton.Connect("button-press-event", func() {
		toolbar.dataAccess.RequestViewChange(common.ViewTypeTop100Result, nil)
	})

	headerBar.Add(storeButton)
	searchBox, err := gtk.SearchEntryNew()
	searchBox.SetMarginTop(3)
	logging.LogCritical(err)
	context, err = searchBox.GetStyleContext()
	logging.LogCritical(err)
	provider, err := gtk.CssProviderNew()
	logging.LogCritical(err)
	err = provider.LoadFromData("*{box-shadow:none; border-radius:1px} ")
	logging.LogCritical(err)
	context.AddProvider(provider, gtk.STYLE_PROVIDER_PRIORITY_USER)

	searchBox.SetMaxWidthChars(29)
	_, _ = searchBox.Connect("search-changed", func() {
		term, _ := searchBox.GetText()
		if term != "" {
			toolbar.dataAccess.RequestViewChange(common.ViewTypeSearchResult, term)
		}
	})
	headerBar.PackEnd(searchBox)
	headerBar.ShowAll()
	toolbar.HeaderBar = headerBar
	toolbar.dataAccess.ConnectViewChanged(func(activeView dataTypes.ViewInterface, navigationStatus *dataTypes.NavigationStatus) {
		toolbar.nextButton.SetSensitive(navigationStatus.CanNavigateForward)
		toolbar.previousButton.SetSensitive(navigationStatus.CanNavigateBack)
	})

	return nil
}
