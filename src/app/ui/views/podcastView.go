package views

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/jtvi/podcast/app/gtkAsyncLoader"
	"gitlab.com/jtvi/podcast/app/library/sourceManager/interfaces"

	"github.com/gotk3/gotk3/pango"
	"gitlab.com/jtvi/podcast/app/library"
	"gitlab.com/jtvi/podcast/app/logging"
	uicommon "gitlab.com/jtvi/podcast/app/ui/common"
	"gitlab.com/jtvi/podcast/app/ui/widgets"
	"gitlab.com/jtvi/podcast/app/utils"
)

type PodcastView struct {
	*gtkAsyncLoader.Controller
	searchResult interfaces.PodcastMetaData
	itunesId     string
	id           string
}

func NewPodcastView(searchResult interfaces.PodcastMetaData) *PodcastView {
	podcastView := new(PodcastView)
	podcastView.itunesId = searchResult.GetId()
	podcastView.id = utils.NewGuidString()
	podcastComponent := NewPodcastComponent(searchResult)
	podcastView.Controller = gtkAsyncLoader.NewController(podcastComponent)
	podcastView.ShowAll()
	return podcastView
}

func (PodcastView) GetViewType() string {
	return string(uicommon.ViewTypePodcastView)
}

func (pv *PodcastView) GetID() string {
	return pv.id
}

type PodcastComponent struct {
	*gtk.Box
	episodesGrid  *widgets.EpisodeGrid
	title         *gtk.Label
	artist        *gtk.Label
	summary       *gtk.Label
	playAllButton *gtk.Button
	imageLoader   *gtkAsyncLoader.Controller
	coverArtUrl   string
	itunesId      string
}

func NewPodcastComponent(searchResult interfaces.PodcastMetaData) *PodcastComponent {
	pv := new(PodcastComponent)
	pv.itunesId = searchResult.GetId()
	pv.coverArtUrl = searchResult.GetCoverArtUrl()

	box, err := gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, 10)
	logging.LogCritical(err)

	box.SetMarginStart(20)
	box.SetMarginTop(20)
	box.SetMarginEnd(20)
	box.SetMarginBottom(20)

	centeredBox, err := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 10)
	logging.LogCritical(err)

	firstRow := widgets.NewPodcastBar(pv.itunesId, pv.coverArtUrl)
	centeredBox.PackStart(firstRow, false, false, 0)

	scrolledWindow, err := gtk.ScrolledWindowNew(nil, nil)
	logging.LogCritical(err)
	scrolledWindow.SetVExpand(true)
	scrolledWindow.SetHExpand(true)
	scrolledWindow.SetPolicy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
	scrolledContent, err := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 0)
	logging.LogCritical(err)
	scrolledContent.SetMarginEnd(20)

	summaryLabel, err := gtk.LabelNew("")
	logging.LogCritical(err)
	summaryLabel.SetMarginTop(0)
	summaryLabel.SetMarginStart(3)
	summaryLabel.SetHAlign(gtk.ALIGN_START)
	summaryLabel.SetVAlign(gtk.ALIGN_START)
	summaryLabel.SetLineWrap(true)
	summaryLabel.SetEllipsize(pango.ELLIPSIZE_START)
	summaryLabel.SetJustify(gtk.JUSTIFY_FILL)
	summaryLabel.SetUseMarkup(true)
	summaryLabel.SetLines(20)
	summaryLabel.SetMarginBottom(20)
	pv.summary = summaryLabel

	scrolledContent.PackStart(summaryLabel, false, false, 10)
	episodeGrid, err := widgets.NewEpisodeGrid()
	logging.LogCritical(err)
	pv.episodesGrid = episodeGrid
	scrolledContent.PackStart(episodeGrid, true, true, 0)
	scrolledWindow.Add(scrolledContent)
	centeredBox.PackStart(scrolledWindow, true, true, 2)
	box.PackStart(centeredBox, true, true, 0)
	pv.Box = box
	return pv
}

func (pv *PodcastComponent) Load(channel *chan interface{}) error {
	itunesPodcast, err := library.GetInstance().GetPodcast(pv.itunesId)
	if err != nil {
		logging.LogError(err)
		return err
	}
	*channel <- itunesPodcast
	return nil
}

func (pv *PodcastComponent) Show(dataInterface interface{}) {
	podcast := dataInterface.(interfaces.PodcastInterface)
	pv.title.SetText(podcast.GetName())
	pv.artist.SetText(podcast.GetArtist())
	pv.summary.SetText(podcast.GetDescription())
	for _, episode := range podcast.GetEpisodes() {
		err := pv.episodesGrid.AddRow(episode)
		logging.LogCritical(err)
	}

}
