package ui

import (
	"errors"
	"gitlab.com/jtvi/podcast/app/dataTypes"
	"gitlab.com/jtvi/podcast/app/library"
	"gitlab.com/jtvi/podcast/app/library/sourceManager/interfaces"
	"gitlab.com/jtvi/podcast/app/logging"
	"gitlab.com/jtvi/podcast/app/player"
	uicommon "gitlab.com/jtvi/podcast/app/ui/common"
	"gitlab.com/jtvi/podcast/app/ui/views"
)

type MainWindowController struct {
	viewHistory          *ViewHistory
	currentView          dataTypes.ViewInterface
	podcastLibrary       *library.Controller
	player               player.ControllerInterface
	viewChangedCallbacks []func(activeView dataTypes.ViewInterface, navigationStatus *dataTypes.NavigationStatus)
}

func NewMainWindowController(podcastLibrary *library.Controller, player player.ControllerInterface) *MainWindowController {
	viewManager := MainWindowController{
		currentView:    nil,
		podcastLibrary: podcastLibrary,
		player:         player,
		viewHistory:    NewViewHistory(),
	}
	return &viewManager
}

func (controller *MainWindowController) Start() {
	//podcastViewRequestChannel := signals.ConnectPodcastViewRequest(nil,1)
	//
	//go func(searchResult interfaces.PodcastMetaData) {
	//	controller.RequestPodcastViewRequest(searchResult)
	//})
	//
	//signals.ConnectSearchViewRequest(nil, func(searchTerm string) {
	//	searchView := views.NewSearchView(searchTerm)
	//	controller.addView(searchView)
	//})
	//
	//signals.ConnectTop100ViewRequest(nil, func() {
	//	top100View := views.NewTop100View()
	//	controller.addView(top100View)
	//
	//})
	//

	controller.RequestViewChange(uicommon.ViewTypeTop100Result, nil)
}

func (controller *MainWindowController) PodcastMetaSelected(podcastMetaData interfaces.PodcastMetaData) {
	controller.RequestViewChange(uicommon.ViewTypePodcastView, podcastMetaData)
}

func (controller *MainWindowController) Search(searchTerm string) ([]interfaces.PodcastMetaData, error) {
	return controller.podcastLibrary.Search(searchTerm)
}

func (controller *MainWindowController) RequestViewChange(viewType uicommon.ViewType, params interface{}) {
	switch viewType {
	case uicommon.ViewTypeTop100Result:
		{
			view := views.NewTop100View(controller)
			controller.addView(view)
		}
	case uicommon.ViewTypeSearchResult:
		{
			view := views.NewSearchView(params.(string), controller)
			controller.addView(view)

		}
	case uicommon.ViewTypePodcastView:
		{
			view := views.NewPodcastView(params.(interfaces.PodcastMetaData))
			controller.addView(view)
		}
	}
}

func (controller *MainWindowController) Navigate(direction dataTypes.NavigationDirection) {
	switch direction {
	case dataTypes.DirectionNext:
		{
			view, ok := controller.viewHistory.GetNext(controller.currentView)
			if !ok {
				return
			}
			controller.currentView = view
		}
	case dataTypes.DirectionPrevious:
		{
			view, ok := controller.viewHistory.GetPrevious(controller.currentView)
			if !ok {
				return
			}
			controller.currentView = view
		}
	default:
		{
			logging.LogCritical(errors.New("unhandled navigation direction"))
		}
	}
}

func (controller *MainWindowController) ConnectViewChanged(callback func(activeView dataTypes.ViewInterface, navigationStatus *dataTypes.NavigationStatus)) {
	controller.viewChangedCallbacks = append(controller.viewChangedCallbacks, callback)
}

func (controller *MainWindowController) addView(view dataTypes.ViewInterface) {
	if view.GetViewType() == string(uicommon.ViewTypeEmptySearchResult) {
		if controller.currentView.GetViewType() == string(uicommon.ViewTypeEmptySearchResult) {
			return
		}
	}
	controller.viewHistory.Insert(view, controller.currentView)
	controller.currentView = view
	controller.triggerViewChange(view)
}

func (controller *MainWindowController) RequestPodcastViewRequest(searchResult interfaces.PodcastMetaData) {
	podcastView := views.NewPodcastView(searchResult)
	controller.addView(podcastView)
}

func (controller *MainWindowController) GetNavigationStatus() *dataTypes.NavigationStatus {
	//currentIndex := controller.viewHistory.getIndex(controller.currentView)
	navigationStatus := new(dataTypes.NavigationStatus)

	//if currentIndex == 0 && controller.viewHistory.Len() > 1 {
	//	navigationStatus.CanNavigateBack = false
	//	navigationStatus.CanNavigateForward = true
	//} else if currentIndex == 0 && controller.viewHistory.Len() <= 1 {
	//	navigationStatus.CanNavigateBack = false
	//	navigationStatus.CanNavigateForward = false
	//} else if currentIndex == controller.viewHistory.Len()-1 {
	//	navigationStatus.CanNavigateBack = true
	//	navigationStatus.CanNavigateForward = false
	//} else {
	//	navigationStatus.CanNavigateBack = true
	//	navigationStatus.CanNavigateForward = true
	//}
	return navigationStatus
}

func (controller *MainWindowController) GetRecommendations() ([]interfaces.PodcastMetaData, error) {
	return controller.podcastLibrary.GetRecommendations()
}

func (controller *MainWindowController) triggerViewChange(view dataTypes.ViewInterface) {
	for _, callback := range controller.viewChangedCallbacks {
		callback(view, controller.GetNavigationStatus())
	}
}
