package widgets

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/jtvi/podcast/app/dataTypes"
	"gitlab.com/jtvi/podcast/app/library/sourceManager/interfaces"
)

//PodcastListView shows podcast_app cover art in a gtk.flowbox
type PodcastListView struct {
	*gtk.ScrolledWindow
	flowbox    *gtk.FlowBox
	items      []*gtk.Box
	controller PodcastListViewController
}

type PodcastListViewController interface {
	PodcastMetaSelected(podcastMetaData interfaces.PodcastMetaData)
}

//NewPodcastFlowbox creates a new instance of a PodcastListView.
func NewPodcastFlowbox(controller PodcastListViewController) (*PodcastListView, error) {
	scrolledWindow, err := gtk.ScrolledWindowNew(nil, nil)
	if err != nil {
		return nil, err
	}
	scrolledWindow.SetVExpand(true)

	flowbox, err := gtk.FlowBoxNew()
	if err != nil {
		return nil, err
	}
	flowbox.SetMarginTop(5)
	flowbox.SetMarginStart(5)
	flowbox.SetMarginEnd(5)
	flowbox.SetMarginBottom(5)
	flowbox.SetHomogeneous(true)
	scrolledWindow.Add(flowbox)

	podcastFlowBox := PodcastListView{
		controller:     controller,
		ScrolledWindow: scrolledWindow,
		flowbox:        flowbox,
		items:          make([]*gtk.Box, 0),
	}
	return &podcastFlowBox, nil
}

//AddPodcasts adds a list of podcasts to PodcastListView.
func (pfb *PodcastListView) AddPodcasts(podcasts []interfaces.PodcastMetaData) (err error) {
	for i := 0; i < len(podcasts); i++ {
		err := pfb.AddPodcast((podcasts)[i])
		if err != nil {
			return err
		}
	}
	return nil
}

//AddPodcast adds a single podcast_app to PodcastListView.
func (pfb *PodcastListView) AddPodcast(searchResult interfaces.PodcastMetaData) (err error) {
	coverArtBox, err := NewPodcastListItem(searchResult, dataTypes.ImageSize{Width: 170, Height: 170})
	coverArtBox.ConnectClicked(func(podcastMetaData interfaces.PodcastMetaData) {
		pfb.controller.PodcastMetaSelected(podcastMetaData)
	})
	if err != nil {
		return err
	}

	pfb.items = append(pfb.items, coverArtBox.Box)
	pfb.flowbox.Add(coverArtBox)
	return nil
}
