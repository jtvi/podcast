package widgets

import (
	"github.com/gotk3/gotk3/gdk"
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/jtvi/podcast/app/dataTypes"
	"gitlab.com/jtvi/podcast/app/library/sourceManager/interfaces"
	"gitlab.com/jtvi/podcast/app/logging"
)

//PodcastListItem holds values for cover art.
type PodcastListItem struct {
	*gtk.Box
	podcastMetaData    interfaces.PodcastMetaData
	imageSize          dataTypes.ImageSize
	title              *gtk.Label
	eventBox           *gtk.EventBox
	image              *gtk.Image
	onClickedCallbacks []func(podcastMetaData interfaces.PodcastMetaData)
}

//NewPodcastListItem creates a new instance of PodcastListItem.
func NewPodcastListItem(podcastMetaData interfaces.PodcastMetaData, imageSize dataTypes.ImageSize) (*PodcastListItem, error) {
	podcastListItem := new(PodcastListItem)
	podcastListItem.imageSize = imageSize
	box, err := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 0)
	if err != nil {
		return nil, err
	}

	title, err := gtk.LabelNewWithMnemonic(podcastMetaData.GetName())
	if err != nil {
		return nil, err
	}
	title.SetLineWrap(true)
	title.SetUseMarkup(false)
	title.SetMaxWidthChars(4)
	title.SetMarginEnd(10)
	title.SetMarginStart(10)

	box.PackEnd(title, false, false, 0)

	podcastListItem.Box = box
	podcastListItem.podcastMetaData = podcastMetaData

	imageOverlay, err := gtk.OverlayNew()
	logging.LogCritical(err)
	image, err := gtk.ImageNew()
	logging.LogCritical(err)

	imageEventBox, err := gtk.EventBoxNew()
	logging.LogCritical(err)

	_, _ = imageEventBox.Connect("enter-notify-event", func() {
		imageEventBox.SetOpacity(0.85)
	})

	_, _ = imageEventBox.Connect("leave-notify-event", func() {
		imageEventBox.SetOpacity(1)
	})

	_, _ = imageEventBox.Connect("button-press-event", func(source *gtk.EventBox, event *gdk.Event) bool {
		eventButton := gdk.EventButtonNewFromEvent(event)
		if eventButton.Button() != 1 {
			return false
		}
		for _, callback := range podcastListItem.onClickedCallbacks {
			callback(podcastListItem.podcastMetaData)
		}
		return true
	})

	imageEventBox.Add(image)
	imageOverlay.Add(imageEventBox)

	imageOverlay.SetSizeRequest(170, 170)
	box.PackStart(imageOverlay, false, false, 10)
	podcastListItem.image = image
	podcastListItem.eventBox = imageEventBox
	podcastListItem.ShowAll()
	go podcastListItem.loadCoverArt()
	return podcastListItem, nil
}

func (podcastListItem *PodcastListItem) loadCoverArt() {
	data, err := podcastListItem.podcastMetaData.GetCoverArt()
	if err != nil {
		//	TODO: handle error
	}
	glib.IdleAdd(podcastListItem._UpdateImage, data)
}

func (podcastListItem *PodcastListItem) ConnectClicked(callback func(podcastMetaData interfaces.PodcastMetaData)) {
	podcastListItem.onClickedCallbacks = append(podcastListItem.onClickedCallbacks, callback)
}

func (podcastListItem *PodcastListItem) _UpdateImage(fileData interfaces.FileInterface) {
	newImage, err := _createImage(fileData.GetFileType(), fileData.GetData(), podcastListItem.imageSize.Width, podcastListItem.imageSize.Height)
	logging.LogCritical(err)
	podcastListItem.eventBox.Remove(podcastListItem.image)
	podcastListItem.eventBox.Add(newImage)
	podcastListItem.eventBox.ShowNow()
}

//CreatePixBuf creates gtk pixbuf from byte data
func _createPixBuf(fileType string, data *[]byte) (*gdk.Pixbuf, error) {
	loader, err := gdk.PixbufLoaderNewWithType(string(fileType))
	if err != nil {
		return nil, err
	}
	d := *data
	_, err = loader.Write(d)
	logging.LogCritical(err)
	err = loader.Close()
	logging.LogCritical(err)
	pixbuf, err := loader.GetPixbuf()
	if err != nil {
		return nil, err
	}
	return pixbuf, nil
}

//createImage creates gtk image from byte data
func _createImage(imageType string, data []byte, x int, y int) (*gtk.Image, error) {
	pixbuf, err := _createPixBuf(imageType, &data)
	if err != nil {
		return nil, err
	}

	pixbuf, err = pixbuf.ScaleSimple(x, y, gdk.INTERP_BILINEAR)
	if err != nil {
		return nil, err
	}

	image, err := gtk.ImageNewFromPixbuf(pixbuf)
	if err != nil {
		return nil, err
	}
	image.Show()
	return image, nil
}
