package widgets

import (
	"github.com/gotk3/gotk3/gdk"
	"github.com/gotk3/gotk3/gtk"
	"github.com/gotk3/gotk3/pango"
	"gitlab.com/jtvi/podcast/app/library/sourceManager/interfaces"
	"gitlab.com/jtvi/podcast/app/logging"
	"gitlab.com/jtvi/podcast/app/translations"
)

type EpisodeGrid struct {
	*gtk.ListBox
	episodes []interfaces.EpisodeInterface
}

func NewEpisodeGrid() (*EpisodeGrid, error) {
	listBox, err := gtk.ListBoxNew()
	if err != nil {
		return nil, err
	}
	episodeGrid := EpisodeGrid{
		ListBox: listBox,
	}
	return &episodeGrid, nil
}

func (episodeGrid *EpisodeGrid) GetEpisodes() []interfaces.EpisodeInterface {
	return episodeGrid.episodes
}

func (episodeGrid *EpisodeGrid) AddRow(episode interfaces.EpisodeInterface) error {
	row, err := gtk.ListBoxRowNew()
	logging.LogCritical(err)
	row.SetHExpand(true)
	row.SetMarginEnd(0)
	row.SetMarginStart(0)

	outerBox, err := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 3)
	logging.LogCritical(err)
	outerBox.SetHExpand(true)

	firstRow, err := gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, 0)
	logging.LogCritical(err)

	nameLabel, err := gtk.LabelNew(episode.GetTitle())
	nameLabel.SetJustify(gtk.JUSTIFY_FILL)
	nameLabel.SetLineWrap(true)
	nameLabel.SetEllipsize(pango.ELLIPSIZE_END)
	nameLabel.SetLines(2)
	nameLabel.SetHExpand(false)
	nameLabel.SetMarginEnd(10)
	nameLabel.SetMarginStart(10)

	logging.LogCritical(err)
	firstRow.PackStart(nameLabel, false, false, 0)

	infoButton, err := gtk.ButtonNewFromIconName("dialog-information", gtk.ICON_SIZE_SMALL_TOOLBAR)
	logging.LogCritical(err)
	infoButton.SetRelief(gtk.RELIEF_NONE)
	infoButton.SetMarginStart(2)
	infoButton.SetMarginEnd(2)

	firstRow.PackEnd(infoButton, false, false, 0)

	dateLabel, err := gtk.LabelNew("-")
	logging.LogCritical(err)
	if episode.GetDate() != nil {
		dateformat := translations.T("dateformat")
		layoutString := episode.GetDate().Format(dateformat)
		dateLabel.SetLabel(layoutString)
	}
	dateLabel.SetSizeRequest(75, 10)
	firstRow.PackEnd(dateLabel, false, false, 0)

	durationLabel, err := gtk.LabelNew("-")
	logging.LogCritical(err)
	if episode.GetDuration() != "" {
		durationLabel.SetLabel(episode.GetDuration())
	}

	durationLabel.SetSizeRequest(75, 10)
	firstRow.PackEnd(durationLabel, false, true, 0)

	progressBar, err := gtk.ProgressBarNew()
	logging.LogCritical(err)
	progressBar.SetVAlign(gtk.ALIGN_CENTER)
	progressBar.SetPulseStep(0.1)
	//id := episode.GetID()
	//signals.ConnectPlaybackPositionPercentChanged(&id, 1)
	//go func(){
	//
	//	progress := float64(positionPercent*1.0) / 1000000
	//	progressBar.SetFraction(float64(progress))
	//})

	firstRow.PackEnd(progressBar, false, false, 0)

	eventBox, err := gtk.EventBoxNew()
	logging.LogCritical(err)

	secondRow, err := gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, 3)
	logging.LogCritical(err)
	secondRow.SetMarginStart(10)
	secondRow.SetMarginEnd(10)
	secondRow.SetVisible(false)

	descriptionLabel, err := gtk.LabelNew(episode.GetDescription())
	logging.LogCritical(err)
	descriptionLabel.SetLineWrap(true)
	descriptionLabel.SetJustify(gtk.JUSTIFY_FILL)
	descriptionLabel.SetMarginStart(15)
	descriptionLabel.SetMarginEnd(15)
	descriptionLabel.Show()

	secondRow.PackStart(descriptionLabel, true, true, 5)

	eventBox.Add(firstRow)
	outerBox.PackStart(eventBox, false, false, 0)
	outerBox.PackStart(secondRow, true, true, 0)

	row.Add(outerBox)
	episodeGrid.ListBox.Add(row)

	episodeGrid.episodes = append(episodeGrid.episodes, episode)

	_, _ = eventBox.Connect("button-press-event", func(sender interface{}, event *gdk.Event) bool {
		eventButton := gdk.EventButtonNewFromEvent(event)
		if eventButton.Type() == 5 {
			rowId := row.GetIndex()
			var episodeList = make([]interfaces.EpisodeInterface, 1)
			episodeList[0] = episodeGrid.episodes[rowId]
			//TODO: Signal
			return true
		}
		return false
	})

	_, _ = infoButton.Connect("button-press-event", func() bool {
		secondRow.SetVisible(!secondRow.GetVisible())
		return true
	})

	firstRow.ShowAll()
	eventBox.Show()
	outerBox.Show()
	row.Show()
	episodeGrid.Show()

	return nil
}
