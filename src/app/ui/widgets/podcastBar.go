package widgets

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/jtvi/podcast/app/dataTypes"
	"gitlab.com/jtvi/podcast/app/logging"
)

type PodcastBar struct {
	*gtk.Box
	artist        *gtk.Label
	title         *gtk.Label
	imageBox      *ImageComponent
	playAllButton *gtk.Button
}

func NewPodcastBar(id string, coverArtUrl string) *PodcastBar {
	widget := new(PodcastBar)

	box, err := gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, 10)
	logging.LogCritical(err)
	params := ImageComponentParameters{
		Id:   id,
		Url:  coverArtUrl,
		Size: dataTypes.ImageSize{210, 210},
	}
	imageBox := NewImageComponent(params)

	widget.imageBox = imageBox
	box.PackStart(imageBox, false, false, 0)

	secondColumn, err := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 0)
	secondColumn.SetMarginBottom(30)
	logging.LogCritical(err)

	buttonBox, err := gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, 0)
	logging.LogCritical(err)

	playAllButton, err := gtk.ButtonNew()
	logging.LogCritical(err)
	playAllButton.SetLabel("Play All")
	playAllButton.Connect("clicked", func() {
		//signals.EmitPlayAll(nil, id)
	})
	widget.playAllButton = playAllButton
	buttonBox.PackStart(playAllButton, false, false, 0)

	followButton, err := gtk.ButtonNew()
	logging.LogCritical(err)
	followButton.SetLabel("Following")
	followButton.SetMarginStart(10)

	secondColumn.PackEnd(buttonBox, false, false, 0)

	artistLabel, err := gtk.LabelNew("")
	logging.LogCritical(err)
	artistLabel.SetMarginTop(5)
	artistLabel.SetUseMarkup(true)
	artistLabel.SetHAlign(gtk.ALIGN_START)
	artistLabel.SetVAlign(gtk.ALIGN_START)
	artistLabel.SetMarginBottom(20)
	widget.artist = artistLabel
	context, err := artistLabel.GetStyleContext()
	logging.LogCritical(err)
	context.AddClass("h2")
	secondColumn.PackEnd(artistLabel, false, false, 0)

	titleLabel, err := gtk.LabelNew("")
	logging.LogCritical(err)
	titleLabel.SetHAlign(gtk.ALIGN_START)
	titleLabel.SetVAlign(gtk.ALIGN_START)
	widget.title = titleLabel
	context, err = titleLabel.GetStyleContext()
	logging.LogCritical(err)
	context.AddClass("h1")
	secondColumn.PackEnd(titleLabel, false, false, 0)

	box.PackStart(secondColumn, true, true, 10)
	widget.Box = box
	return widget
}
