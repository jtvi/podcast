package widgets

import (
	"github.com/gotk3/gotk3/gdk"
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/jtvi/podcast/app/dataTypes"
	"gitlab.com/jtvi/podcast/app/library/sourceManager/interfaces"
	"gitlab.com/jtvi/podcast/app/logging"
)

type ImageComponent struct {
	*gtk.Overlay
	eventBox    *gtk.EventBox
	image       *gtk.Image
	clickable   bool
	size        dataTypes.ImageSize
	coverArtUrl string
	id          string
}

type ImageComponentParameters struct {
	Id        string
	Url       string
	Size      dataTypes.ImageSize
	Clickable bool
}

func NewImageComponent(params ImageComponentParameters) *ImageComponent {
	imageBox := new(ImageComponent)
	imageBox.clickable = params.Clickable
	imageBox.size = params.Size
	imageBox.coverArtUrl = params.Url
	imageBox.id = params.Id

	imageOverlay, err := gtk.OverlayNew()
	logging.LogCritical(err)
	imageBox.Overlay = imageOverlay
	image, err := gtk.ImageNew()
	logging.LogCritical(err)
	imageBox.image = image

	if imageBox.clickable {
		imageEventBox, err := gtk.EventBoxNew()
		logging.LogCritical(err)

		_, _ = imageEventBox.Connect("enter-notify-event", func() {
			imageEventBox.SetOpacity(0.85)
		})

		_, _ = imageEventBox.Connect("leave-notify-event", func() {
			imageEventBox.SetOpacity(1)
		})

		_, _ = imageEventBox.Connect("button-press-event", func(source *gtk.EventBox, event *gdk.Event) bool {
			eventButton := gdk.EventButtonNewFromEvent(event)
			if eventButton.Button() != 1 {
				return false
			}
			//signals.EmitImageboxClicked(&imageBox.id)
			return true
		})
		imageBox.eventBox = imageEventBox
		imageBox.eventBox.Add(image)
		imageOverlay.Add(imageEventBox)
	} else {
		imageOverlay.Add(image)
	}
	imageBox.Overlay = imageOverlay
	return imageBox
}

func (imageContent *ImageComponent) UpdateImage(fileData interfaces.FileInterface) {
	newImage, err := createImage(fileData.GetFileType(), fileData.GetData(), imageContent.size.Width, imageContent.size.Height)
	logging.LogCritical(err)
	if imageContent.clickable {
		imageContent.eventBox.Remove(imageContent.image)
		imageContent.eventBox.Add(newImage)
		imageContent.eventBox.ShowNow()

	} else {
		imageContent.Remove(imageContent.image)
		imageContent.Add(newImage)
		imageContent.ShowAll()
	}
}

//CreatePixBuf creates gtk pixbuf from byte data
func createPixBuf(fileType string, data *[]byte) (*gdk.Pixbuf, error) {
	loader, err := gdk.PixbufLoaderNewWithType(string(fileType))
	if err != nil {
		return nil, err
	}
	d := *data
	_, err = loader.Write(d)
	logging.LogCritical(err)
	err = loader.Close()
	logging.LogCritical(err)
	pixbuf, err := loader.GetPixbuf()
	if err != nil {
		return nil, err
	}
	return pixbuf, nil
}

//createImage creates gtk image from byte data
func createImage(imageType string, data []byte, x int, y int) (*gtk.Image, error) {
	pixbuf, err := createPixBuf(imageType, &data)
	if err != nil {
		return nil, err
	}

	pixbuf, err = pixbuf.ScaleSimple(x, y, gdk.INTERP_BILINEAR)
	if err != nil {
		return nil, err
	}

	image, err := gtk.ImageNewFromPixbuf(pixbuf)
	if err != nil {
		return nil, err
	}
	image.Show()
	return image, nil
}
