package gtkAsyncLoader

import "github.com/gotk3/gotk3/gtk"

type ControllerInterface interface {
	gtk.IWidget
	Load(*chan interface{}) error
	Show(interface{})
}
