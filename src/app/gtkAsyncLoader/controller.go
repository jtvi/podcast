package gtkAsyncLoader

import (
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/jtvi/podcast/app/logging"
)

type ComponentName string

const (
	componentNameLoading ComponentName = "componentNameLoading"
	componentNameMain    ComponentName = "main"
	componentNameError   ComponentName = "error"
)

const MAXCOUNT int = 3

type Controller struct {
	*gtk.Stack
	widgetInterface ControllerInterface
	loadingView     gtk.Widget
	ErrorView       *ErrorView
}

func NewController(componentInterface ControllerInterface) *Controller {
	gtkAsyncLoader := new(Controller)

	stack, err := gtk.StackNew()
	logging.LogCritical(err)
	gtkAsyncLoader.Stack = stack

	gtkAsyncLoader.widgetInterface = componentInterface
	gtkAsyncLoader.AddNamed(gtkAsyncLoader.widgetInterface, string(componentNameMain))
	gtkAsyncLoader.AddNamed(NewLoadingView(), string(componentNameLoading))
	gtkAsyncLoader.ErrorView = NewErrorView()
	gtkAsyncLoader.AddNamed(gtkAsyncLoader.ErrorView, string(componentNameError))

	gtkAsyncLoader.start()

	return gtkAsyncLoader
}

func (gtkAsyncLoader *Controller) start() {
	go func() {
		glib.IdleAdd(gtkAsyncLoader.SetVisibleChildName, string(componentNameLoading))
		count := 1
		chn := make(chan interface{}, 1)
		var err error = nil
		for count <= MAXCOUNT {
			err = gtkAsyncLoader.widgetInterface.Load(&chn)
			if err == nil {
				break
			}
			count = count + 1
		}

		if err == nil {
			data := updateContainer{
				&chn,
				gtkAsyncLoader.widgetInterface.Show,
			}
			glib.IdleAdd(gtkAsyncLoader.updateMethod, data)

		} else {
			glib.IdleAdd(gtkAsyncLoader.ErrorView.SetError, err)
			glib.IdleAdd(gtkAsyncLoader.SetVisibleChildName, string(componentNameError))
			glib.IdleAdd(gtkAsyncLoader.Show, nil)
		}

		close(chn)
	}()
}

type updateContainer struct {
	Channel    *chan interface{}
	UpdateFunc func(interface{})
}

func (gtkAsyncLoader *Controller) updateMethod(data updateContainer) bool {
	select {
	case value, ok := <-*data.Channel:
		if ok {
			gtkAsyncLoader.widgetInterface.Show(value)
			gtkAsyncLoader.SetVisibleChildName(string(componentNameMain))
			gtkAsyncLoader.Show()
			return true
		} else {
			return false
		}
	}
}
