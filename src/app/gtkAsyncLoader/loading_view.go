package gtkAsyncLoader

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/jtvi/podcast/app/logging"
	"gitlab.com/jtvi/podcast/app/translations"
)

type LoadingView struct {
	*gtk.Box
}

func NewLoadingView() *LoadingView {
	loadingView := new(LoadingView)
	box, err := gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, 5)

	logging.LogCritical(err)

	label, err := gtk.LabelNew(translations.T("Loading..."))
	logging.LogCritical(err)

	box.SetCenterWidget(label)
	loadingView.Box = box
	return loadingView
}
