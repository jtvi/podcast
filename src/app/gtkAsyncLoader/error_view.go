package gtkAsyncLoader

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/jtvi/podcast/app/logging"
)

type ErrorView struct {
	*gtk.Box
	errorLabel *gtk.Label
}

func NewErrorView() *ErrorView {
	errorView := new(ErrorView)
	box, err := gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, 5)
	logging.LogCritical(err)
	label, err := gtk.LabelNew("")
	errorView.errorLabel = label
	logging.LogCritical(err)

	box.SetCenterWidget(label)
	box.Show()
	errorView.Box = box
	return errorView
}

func (ew *ErrorView) SetError(appError error) {
	ew.errorLabel.SetText(appError.Error())
	ew.errorLabel.Show()
}
