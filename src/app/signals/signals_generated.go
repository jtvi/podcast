package signals

import (
	"fmt"
	"gitlab.com/jtvi/podcast/app/dataTypes"
	"gitlab.com/jtvi/podcast/app/library/sourceManager/interfaces"
	"sync"
)

var handler *eventHandler
var once sync.Once

type Top100ViewRequestResponseParams struct {
}
type SearchViewRequestResponseParams struct {
	SearchTerm string
}
type LibraryViewRequestResponseParams struct {
}
type PodcastViewRequestResponseParams struct {
	SearchResult interfaces.PodcastMetaData
}
type ViewChangedResponseParams struct {
	ViewId           string
	View             dataTypes.ViewInterface
	NavigationStatus *dataTypes.NavigationStatus
}
type NavigationEventResponseParams struct {
	Direction dataTypes.NavigationDirection
}
type ImageboxClickedResponseParams struct {
}
type ViewHistoryChangedResponseParams struct {
	Views []dataTypes.ViewInterface
}
type PlayAllResponseParams struct {
	PodcastId string
}
type TogglePlayResponseParams struct {
}
type PlaybackChangedResponseParams struct {
	Status dataTypes.Status
}
type PlayNextResponseParams struct {
}
type PlayPreviousResponseParams struct {
}
type SetVolumeResponseParams struct {
	Value float64
}
type VolumeChangedResponseParams struct {
	Value float64
}
type SeekResponseParams struct {
	Value float64
}
type PlaybackPositionChangedResponseParams struct {
	PositionTime int64
	DurationTime int64
}
type PlaybackPositionPercentChangedResponseParams struct {
	PositionPercent int64
}
type SettingsChangedResponseParams struct {
	Value interface{}
}

type eventHandler struct {
	Top100ViewRequestChannels              map[string][]chan Top100ViewRequestResponseParams
	SearchViewRequestChannels              map[string][]chan SearchViewRequestResponseParams
	LibraryViewRequestChannels             map[string][]chan LibraryViewRequestResponseParams
	PodcastViewRequestChannels             map[string][]chan PodcastViewRequestResponseParams
	ViewChangedChannels                    map[string][]chan ViewChangedResponseParams
	NavigationEventChannels                map[string][]chan NavigationEventResponseParams
	ImageboxClickedChannels                map[string][]chan ImageboxClickedResponseParams
	ViewHistoryChangedChannels             map[string][]chan ViewHistoryChangedResponseParams
	PlayAllChannels                        map[string][]chan PlayAllResponseParams
	TogglePlayChannels                     map[string][]chan TogglePlayResponseParams
	PlaybackChangedChannels                map[string][]chan PlaybackChangedResponseParams
	PlayNextChannels                       map[string][]chan PlayNextResponseParams
	PlayPreviousChannels                   map[string][]chan PlayPreviousResponseParams
	SetVolumeChannels                      map[string][]chan SetVolumeResponseParams
	VolumeChangedChannels                  map[string][]chan VolumeChangedResponseParams
	SeekChannels                           map[string][]chan SeekResponseParams
	PlaybackPositionChangedChannels        map[string][]chan PlaybackPositionChangedResponseParams
	PlaybackPositionPercentChangedChannels map[string][]chan PlaybackPositionPercentChangedResponseParams
	SettingsChangedChannels                map[string][]chan SettingsChangedResponseParams
}

func Init(defaultChannelSize uint8) {
	once.Do(func() {
		handler = &eventHandler{

			Top100ViewRequestChannels:              make(map[string][]chan Top100ViewRequestResponseParams, 0),
			SearchViewRequestChannels:              make(map[string][]chan SearchViewRequestResponseParams, 0),
			LibraryViewRequestChannels:             make(map[string][]chan LibraryViewRequestResponseParams, 0),
			PodcastViewRequestChannels:             make(map[string][]chan PodcastViewRequestResponseParams, 0),
			ViewChangedChannels:                    make(map[string][]chan ViewChangedResponseParams, 0),
			NavigationEventChannels:                make(map[string][]chan NavigationEventResponseParams, 0),
			ImageboxClickedChannels:                make(map[string][]chan ImageboxClickedResponseParams, 0),
			ViewHistoryChangedChannels:             make(map[string][]chan ViewHistoryChangedResponseParams, 0),
			PlayAllChannels:                        make(map[string][]chan PlayAllResponseParams, 0),
			TogglePlayChannels:                     make(map[string][]chan TogglePlayResponseParams, 0),
			PlaybackChangedChannels:                make(map[string][]chan PlaybackChangedResponseParams, 0),
			PlayNextChannels:                       make(map[string][]chan PlayNextResponseParams, 0),
			PlayPreviousChannels:                   make(map[string][]chan PlayPreviousResponseParams, 0),
			SetVolumeChannels:                      make(map[string][]chan SetVolumeResponseParams, 0),
			VolumeChangedChannels:                  make(map[string][]chan VolumeChangedResponseParams, 0),
			SeekChannels:                           make(map[string][]chan SeekResponseParams, 0),
			PlaybackPositionChangedChannels:        make(map[string][]chan PlaybackPositionChangedResponseParams, 0),
			PlaybackPositionPercentChangedChannels: make(map[string][]chan PlaybackPositionPercentChangedResponseParams, 0),
			SettingsChangedChannels:                make(map[string][]chan SettingsChangedResponseParams, 0),
		}
		handler.Top100ViewRequestChannels["Top100ViewRequest"] = make([]chan Top100ViewRequestResponseParams, defaultChannelSize)

		handler.SearchViewRequestChannels["SearchViewRequest"] = make([]chan SearchViewRequestResponseParams, defaultChannelSize)

		handler.LibraryViewRequestChannels["LibraryViewRequest"] = make([]chan LibraryViewRequestResponseParams, defaultChannelSize)

		handler.PodcastViewRequestChannels["PodcastViewRequest"] = make([]chan PodcastViewRequestResponseParams, defaultChannelSize)

		handler.ViewChangedChannels["ViewChanged"] = make([]chan ViewChangedResponseParams, defaultChannelSize)

		handler.NavigationEventChannels["NavigationEvent"] = make([]chan NavigationEventResponseParams, defaultChannelSize)

		handler.ImageboxClickedChannels["ImageboxClicked"] = make([]chan ImageboxClickedResponseParams, defaultChannelSize)

		handler.ViewHistoryChangedChannels["ViewHistoryChanged"] = make([]chan ViewHistoryChangedResponseParams, defaultChannelSize)

		handler.PlayAllChannels["PlayAll"] = make([]chan PlayAllResponseParams, defaultChannelSize)

		handler.TogglePlayChannels["TogglePlay"] = make([]chan TogglePlayResponseParams, defaultChannelSize)

		handler.PlaybackChangedChannels["PlaybackChanged"] = make([]chan PlaybackChangedResponseParams, defaultChannelSize)

		handler.PlayNextChannels["PlayNext"] = make([]chan PlayNextResponseParams, defaultChannelSize)

		handler.PlayPreviousChannels["PlayPrevious"] = make([]chan PlayPreviousResponseParams, defaultChannelSize)

		handler.SetVolumeChannels["SetVolume"] = make([]chan SetVolumeResponseParams, defaultChannelSize)

		handler.VolumeChangedChannels["VolumeChanged"] = make([]chan VolumeChangedResponseParams, defaultChannelSize)

		handler.SeekChannels["Seek"] = make([]chan SeekResponseParams, defaultChannelSize)

		handler.PlaybackPositionChangedChannels["PlaybackPositionChanged"] = make([]chan PlaybackPositionChangedResponseParams, defaultChannelSize)

		handler.PlaybackPositionPercentChangedChannels["PlaybackPositionPercentChanged"] = make([]chan PlaybackPositionPercentChangedResponseParams, defaultChannelSize)

		handler.SettingsChangedChannels["SettingsChanged"] = make([]chan SettingsChangedResponseParams, defaultChannelSize)

	})
}

func EmitTop100ViewRequest(id *string) {
	if id != nil {
		channelsById := handler.Top100ViewRequestChannels[*id]
		for _, channel := range channelsById {
			params := Top100ViewRequestResponseParams{}
			select {
			case channel <- params:
			default:
				fmt.Printf("channel full for %v - skipping value %v", *id, params)
			}
		}
	}
	channels := handler.Top100ViewRequestChannels["Top100ViewRequest"]

	params := Top100ViewRequestResponseParams{}
	select {
	case channels[0] <- params:
	default:
		fmt.Printf("channel full for Top100ViewRequest - skipping value %v", params)
	}

}

func ConnectTop100ViewRequest(id *string, channelSize uint8) chan Top100ViewRequestResponseParams {
	connectId := "Top100ViewRequest"
	if id != nil {
		connectId = *id
		channel := make(chan Top100ViewRequestResponseParams, channelSize)
		channelList := handler.Top100ViewRequestChannels[connectId]
		channelList = append(channelList, channel)
		handler.Top100ViewRequestChannels[connectId] = channelList
		return channel
	}
	channel := handler.Top100ViewRequestChannels[connectId]
	return channel[0]
}

func EmitSearchViewRequest(id *string, searchTerm string) {
	if id != nil {
		channelsById := handler.SearchViewRequestChannels[*id]
		for _, channel := range channelsById {
			params := SearchViewRequestResponseParams{
				SearchTerm: searchTerm,
			}
			select {
			case channel <- params:
			default:
				fmt.Printf("channel full for %v - skipping value %v", *id, params)
			}
		}
	}
	channels := handler.SearchViewRequestChannels["SearchViewRequest"]

	params := SearchViewRequestResponseParams{
		SearchTerm: searchTerm,
	}
	select {
	case channels[0] <- params:
	default:
		fmt.Printf("channel full for SearchViewRequest - skipping value %v", params)
	}

}

func ConnectSearchViewRequest(id *string, channelSize uint8) chan SearchViewRequestResponseParams {
	connectId := "SearchViewRequest"
	if id != nil {
		connectId = *id
		channel := make(chan SearchViewRequestResponseParams, channelSize)
		channelList := handler.SearchViewRequestChannels[connectId]
		channelList = append(channelList, channel)
		handler.SearchViewRequestChannels[connectId] = channelList
		return channel
	}
	channel := handler.SearchViewRequestChannels[connectId]
	return channel[0]
}

func EmitLibraryViewRequest(id *string) {
	if id != nil {
		channelsById := handler.LibraryViewRequestChannels[*id]
		for _, channel := range channelsById {
			params := LibraryViewRequestResponseParams{}
			select {
			case channel <- params:
			default:
				fmt.Printf("channel full for %v - skipping value %v", *id, params)
			}
		}
	}
	channels := handler.LibraryViewRequestChannels["LibraryViewRequest"]

	params := LibraryViewRequestResponseParams{}
	select {
	case channels[0] <- params:
	default:
		fmt.Printf("channel full for LibraryViewRequest - skipping value %v", params)
	}

}

func ConnectLibraryViewRequest(id *string, channelSize uint8) chan LibraryViewRequestResponseParams {
	connectId := "LibraryViewRequest"
	if id != nil {
		connectId = *id
		channel := make(chan LibraryViewRequestResponseParams, channelSize)
		channelList := handler.LibraryViewRequestChannels[connectId]
		channelList = append(channelList, channel)
		handler.LibraryViewRequestChannels[connectId] = channelList
		return channel
	}
	channel := handler.LibraryViewRequestChannels[connectId]
	return channel[0]
}

func EmitPodcastViewRequest(id *string, searchResult interfaces.PodcastMetaData) {
	if id != nil {
		channelsById := handler.PodcastViewRequestChannels[*id]
		for _, channel := range channelsById {
			params := PodcastViewRequestResponseParams{
				SearchResult: searchResult,
			}
			select {
			case channel <- params:
			default:
				fmt.Printf("channel full for %v - skipping value %v", *id, params)
			}
		}
	}
	channels := handler.PodcastViewRequestChannels["PodcastViewRequest"]

	params := PodcastViewRequestResponseParams{
		SearchResult: searchResult,
	}
	select {
	case channels[0] <- params:
	default:
		fmt.Printf("channel full for PodcastViewRequest - skipping value %v", params)
	}

}

func ConnectPodcastViewRequest(id *string, channelSize uint8) chan PodcastViewRequestResponseParams {
	connectId := "PodcastViewRequest"
	if id != nil {
		connectId = *id
		channel := make(chan PodcastViewRequestResponseParams, channelSize)
		channelList := handler.PodcastViewRequestChannels[connectId]
		channelList = append(channelList, channel)
		handler.PodcastViewRequestChannels[connectId] = channelList
		return channel
	}
	channel := handler.PodcastViewRequestChannels[connectId]
	return channel[0]
}

func EmitViewChanged(id *string, viewId string, view dataTypes.ViewInterface, navigationStatus *dataTypes.NavigationStatus) {
	if id != nil {
		channelsById := handler.ViewChangedChannels[*id]
		for _, channel := range channelsById {
			params := ViewChangedResponseParams{
				ViewId:           viewId,
				View:             view,
				NavigationStatus: navigationStatus,
			}
			select {
			case channel <- params:
			default:
				fmt.Printf("channel full for %v - skipping value %v", *id, params)
			}
		}
	}
	channels := handler.ViewChangedChannels["ViewChanged"]

	params := ViewChangedResponseParams{
		ViewId:           viewId,
		View:             view,
		NavigationStatus: navigationStatus,
	}
	select {
	case channels[0] <- params:
	default:
		fmt.Printf("channel full for ViewChanged - skipping value %v", params)
	}

}

func ConnectViewChanged(id *string, channelSize uint8) chan ViewChangedResponseParams {
	connectId := "ViewChanged"
	if id != nil {
		connectId = *id
		channel := make(chan ViewChangedResponseParams, channelSize)
		channelList := handler.ViewChangedChannels[connectId]
		channelList = append(channelList, channel)
		handler.ViewChangedChannels[connectId] = channelList
		return channel
	}
	channel := handler.ViewChangedChannels[connectId]
	return channel[0]
}

func EmitNavigationEvent(id *string, direction dataTypes.NavigationDirection) {
	if id != nil {
		channelsById := handler.NavigationEventChannels[*id]
		for _, channel := range channelsById {
			params := NavigationEventResponseParams{
				Direction: direction,
			}
			select {
			case channel <- params:
			default:
				fmt.Printf("channel full for %v - skipping value %v", *id, params)
			}
		}
	}
	channels := handler.NavigationEventChannels["NavigationEvent"]

	params := NavigationEventResponseParams{
		Direction: direction,
	}
	select {
	case channels[0] <- params:
	default:
		fmt.Printf("channel full for NavigationEvent - skipping value %v", params)
	}

}

func ConnectNavigationEvent(id *string, channelSize uint8) chan NavigationEventResponseParams {
	connectId := "NavigationEvent"
	if id != nil {
		connectId = *id
		channel := make(chan NavigationEventResponseParams, channelSize)
		channelList := handler.NavigationEventChannels[connectId]
		channelList = append(channelList, channel)
		handler.NavigationEventChannels[connectId] = channelList
		return channel
	}
	channel := handler.NavigationEventChannels[connectId]
	return channel[0]
}

func EmitImageboxClicked(id *string) {
	if id != nil {
		channelsById := handler.ImageboxClickedChannels[*id]
		for _, channel := range channelsById {
			params := ImageboxClickedResponseParams{}
			select {
			case channel <- params:
			default:
				fmt.Printf("channel full for %v - skipping value %v", *id, params)
			}
		}
	}
	channels := handler.ImageboxClickedChannels["ImageboxClicked"]

	params := ImageboxClickedResponseParams{}
	select {
	case channels[0] <- params:
	default:
		fmt.Printf("channel full for ImageboxClicked - skipping value %v", params)
	}

}

func ConnectImageboxClicked(id *string, channelSize uint8) chan ImageboxClickedResponseParams {
	connectId := "ImageboxClicked"
	if id != nil {
		connectId = *id
		channel := make(chan ImageboxClickedResponseParams, channelSize)
		channelList := handler.ImageboxClickedChannels[connectId]
		channelList = append(channelList, channel)
		handler.ImageboxClickedChannels[connectId] = channelList
		return channel
	}
	channel := handler.ImageboxClickedChannels[connectId]
	return channel[0]
}

func EmitViewHistoryChanged(id *string, views []dataTypes.ViewInterface) {
	if id != nil {
		channelsById := handler.ViewHistoryChangedChannels[*id]
		for _, channel := range channelsById {
			params := ViewHistoryChangedResponseParams{
				Views: views,
			}
			select {
			case channel <- params:
			default:
				fmt.Printf("channel full for %v - skipping value %v", *id, params)
			}
		}
	}
	channels := handler.ViewHistoryChangedChannels["ViewHistoryChanged"]

	params := ViewHistoryChangedResponseParams{
		Views: views,
	}
	select {
	case channels[0] <- params:
	default:
		fmt.Printf("channel full for ViewHistoryChanged - skipping value %v", params)
	}

}

func ConnectViewHistoryChanged(id *string, channelSize uint8) chan ViewHistoryChangedResponseParams {
	connectId := "ViewHistoryChanged"
	if id != nil {
		connectId = *id
		channel := make(chan ViewHistoryChangedResponseParams, channelSize)
		channelList := handler.ViewHistoryChangedChannels[connectId]
		channelList = append(channelList, channel)
		handler.ViewHistoryChangedChannels[connectId] = channelList
		return channel
	}
	channel := handler.ViewHistoryChangedChannels[connectId]
	return channel[0]
}

func EmitPlayAll(id *string, podcastId string) {
	if id != nil {
		channelsById := handler.PlayAllChannels[*id]
		for _, channel := range channelsById {
			params := PlayAllResponseParams{
				PodcastId: podcastId,
			}
			select {
			case channel <- params:
			default:
				fmt.Printf("channel full for %v - skipping value %v", *id, params)
			}
		}
	}
	channels := handler.PlayAllChannels["PlayAll"]

	params := PlayAllResponseParams{
		PodcastId: podcastId,
	}
	select {
	case channels[0] <- params:
	default:
		fmt.Printf("channel full for PlayAll - skipping value %v", params)
	}

}

func ConnectPlayAll(id *string, channelSize uint8) chan PlayAllResponseParams {
	connectId := "PlayAll"
	if id != nil {
		connectId = *id
		channel := make(chan PlayAllResponseParams, channelSize)
		channelList := handler.PlayAllChannels[connectId]
		channelList = append(channelList, channel)
		handler.PlayAllChannels[connectId] = channelList
		return channel
	}
	channel := handler.PlayAllChannels[connectId]
	return channel[0]
}

func EmitTogglePlay(id *string) {
	if id != nil {
		channelsById := handler.TogglePlayChannels[*id]
		for _, channel := range channelsById {
			params := TogglePlayResponseParams{}
			select {
			case channel <- params:
			default:
				fmt.Printf("channel full for %v - skipping value %v", *id, params)
			}
		}
	}
	channels := handler.TogglePlayChannels["TogglePlay"]

	params := TogglePlayResponseParams{}
	select {
	case channels[0] <- params:
	default:
		fmt.Printf("channel full for TogglePlay - skipping value %v", params)
	}

}

func ConnectTogglePlay(id *string, channelSize uint8) chan TogglePlayResponseParams {
	connectId := "TogglePlay"
	if id != nil {
		connectId = *id
		channel := make(chan TogglePlayResponseParams, channelSize)
		channelList := handler.TogglePlayChannels[connectId]
		channelList = append(channelList, channel)
		handler.TogglePlayChannels[connectId] = channelList
		return channel
	}
	channel := handler.TogglePlayChannels[connectId]
	return channel[0]
}

func EmitPlaybackChanged(id *string, status dataTypes.Status) {
	if id != nil {
		channelsById := handler.PlaybackChangedChannels[*id]
		for _, channel := range channelsById {
			params := PlaybackChangedResponseParams{
				Status: status,
			}
			select {
			case channel <- params:
			default:
				fmt.Printf("channel full for %v - skipping value %v", *id, params)
			}
		}
	}
	channels := handler.PlaybackChangedChannels["PlaybackChanged"]

	params := PlaybackChangedResponseParams{
		Status: status,
	}
	select {
	case channels[0] <- params:
	default:
		fmt.Printf("channel full for PlaybackChanged - skipping value %v", params)
	}

}

func ConnectPlaybackChanged(id *string, channelSize uint8) chan PlaybackChangedResponseParams {
	connectId := "PlaybackChanged"
	if id != nil {
		connectId = *id
		channel := make(chan PlaybackChangedResponseParams, channelSize)
		channelList := handler.PlaybackChangedChannels[connectId]
		channelList = append(channelList, channel)
		handler.PlaybackChangedChannels[connectId] = channelList
		return channel
	}
	channel := handler.PlaybackChangedChannels[connectId]
	return channel[0]
}

func EmitPlayNext(id *string) {
	if id != nil {
		channelsById := handler.PlayNextChannels[*id]
		for _, channel := range channelsById {
			params := PlayNextResponseParams{}
			select {
			case channel <- params:
			default:
				fmt.Printf("channel full for %v - skipping value %v", *id, params)
			}
		}
	}
	channels := handler.PlayNextChannels["PlayNext"]

	params := PlayNextResponseParams{}
	select {
	case channels[0] <- params:
	default:
		fmt.Printf("channel full for PlayNext - skipping value %v", params)
	}

}

func ConnectPlayNext(id *string, channelSize uint8) chan PlayNextResponseParams {
	connectId := "PlayNext"
	if id != nil {
		connectId = *id
		channel := make(chan PlayNextResponseParams, channelSize)
		channelList := handler.PlayNextChannels[connectId]
		channelList = append(channelList, channel)
		handler.PlayNextChannels[connectId] = channelList
		return channel
	}
	channel := handler.PlayNextChannels[connectId]
	return channel[0]
}

func EmitPlayPrevious(id *string) {
	if id != nil {
		channelsById := handler.PlayPreviousChannels[*id]
		for _, channel := range channelsById {
			params := PlayPreviousResponseParams{}
			select {
			case channel <- params:
			default:
				fmt.Printf("channel full for %v - skipping value %v", *id, params)
			}
		}
	}
	channels := handler.PlayPreviousChannels["PlayPrevious"]

	params := PlayPreviousResponseParams{}
	select {
	case channels[0] <- params:
	default:
		fmt.Printf("channel full for PlayPrevious - skipping value %v", params)
	}

}

func ConnectPlayPrevious(id *string, channelSize uint8) chan PlayPreviousResponseParams {
	connectId := "PlayPrevious"
	if id != nil {
		connectId = *id
		channel := make(chan PlayPreviousResponseParams, channelSize)
		channelList := handler.PlayPreviousChannels[connectId]
		channelList = append(channelList, channel)
		handler.PlayPreviousChannels[connectId] = channelList
		return channel
	}
	channel := handler.PlayPreviousChannels[connectId]
	return channel[0]
}

func EmitSetVolume(id *string, value float64) {
	if id != nil {
		channelsById := handler.SetVolumeChannels[*id]
		for _, channel := range channelsById {
			params := SetVolumeResponseParams{
				Value: value,
			}
			select {
			case channel <- params:
			default:
				fmt.Printf("channel full for %v - skipping value %v", *id, params)
			}
		}
	}
	channels := handler.SetVolumeChannels["SetVolume"]

	params := SetVolumeResponseParams{
		Value: value,
	}
	select {
	case channels[0] <- params:
	default:
		fmt.Printf("channel full for SetVolume - skipping value %v", params)
	}

}

func ConnectSetVolume(id *string, channelSize uint8) chan SetVolumeResponseParams {
	connectId := "SetVolume"
	if id != nil {
		connectId = *id
		channel := make(chan SetVolumeResponseParams, channelSize)
		channelList := handler.SetVolumeChannels[connectId]
		channelList = append(channelList, channel)
		handler.SetVolumeChannels[connectId] = channelList
		return channel
	}
	channel := handler.SetVolumeChannels[connectId]
	return channel[0]
}

func EmitVolumeChanged(id *string, value float64) {
	if id != nil {
		channelsById := handler.VolumeChangedChannels[*id]
		for _, channel := range channelsById {
			params := VolumeChangedResponseParams{
				Value: value,
			}
			select {
			case channel <- params:
			default:
				fmt.Printf("channel full for %v - skipping value %v", *id, params)
			}
		}
	}
	//	channels := handler.VolumeChangedChannels["VolumeChanged"]

	//params := VolumeChangedResponseParams{
	//	Value: value,
	//}
	//select {
	//case channels[0] <- params:
	//default:
	//	fmt.Printf("channel full for VolumeChanged - skipping value %v", params)
	//}

}

func ConnectVolumeChanged(id *string, channelSize uint8) chan VolumeChangedResponseParams {
	connectId := "VolumeChanged"
	if id != nil {
		connectId = *id
		channel := make(chan VolumeChangedResponseParams, channelSize)
		channelList := handler.VolumeChangedChannels[connectId]
		channelList = append(channelList, channel)
		handler.VolumeChangedChannels[connectId] = channelList
		return channel
	}
	channel := handler.VolumeChangedChannels[connectId]
	return channel[0]
}

func EmitSeek(id *string, value float64) {
	if id != nil {
		channelsById := handler.SeekChannels[*id]
		for _, channel := range channelsById {
			params := SeekResponseParams{
				Value: value,
			}
			select {
			case channel <- params:
			default:
				fmt.Printf("channel full for %v - skipping value %v", *id, params)
			}
		}
	}
	channels := handler.SeekChannels["Seek"]

	params := SeekResponseParams{
		Value: value,
	}
	select {
	case channels[0] <- params:
	default:
		fmt.Printf("channel full for Seek - skipping value %v", params)
	}

}

func ConnectSeek(id *string, channelSize uint8) chan SeekResponseParams {
	connectId := "Seek"
	if id != nil {
		connectId = *id
		channel := make(chan SeekResponseParams, channelSize)
		channelList := handler.SeekChannels[connectId]
		channelList = append(channelList, channel)
		handler.SeekChannels[connectId] = channelList
		return channel
	}
	channel := handler.SeekChannels[connectId]
	return channel[0]
}

func EmitPlaybackPositionChanged(id *string, positionTime int64, durationTime int64) {
	if id != nil {
		channelsById := handler.PlaybackPositionChangedChannels[*id]
		for _, channel := range channelsById {
			params := PlaybackPositionChangedResponseParams{
				PositionTime: positionTime,
				DurationTime: durationTime,
			}
			select {
			case channel <- params:
			default:
				fmt.Printf("channel full for %v - skipping value %v", *id, params)
			}
		}
	}
	channels := handler.PlaybackPositionChangedChannels["PlaybackPositionChanged"]

	params := PlaybackPositionChangedResponseParams{
		PositionTime: positionTime,
		DurationTime: durationTime,
	}
	select {
	case channels[0] <- params:
	default:
		fmt.Printf("channel full for PlaybackPositionChanged - skipping value %v", params)
	}

}

func ConnectPlaybackPositionChanged(id *string, channelSize uint8) chan PlaybackPositionChangedResponseParams {
	connectId := "PlaybackPositionChanged"
	if id != nil {
		connectId = *id
		channel := make(chan PlaybackPositionChangedResponseParams, channelSize)
		channelList := handler.PlaybackPositionChangedChannels[connectId]
		channelList = append(channelList, channel)
		handler.PlaybackPositionChangedChannels[connectId] = channelList
		return channel
	}
	channel := handler.PlaybackPositionChangedChannels[connectId]
	return channel[0]
}

func EmitPlaybackPositionPercentChanged(id *string, positionPercent int64) {
	if id != nil {
		channelsById := handler.PlaybackPositionPercentChangedChannels[*id]
		for _, channel := range channelsById {
			params := PlaybackPositionPercentChangedResponseParams{
				PositionPercent: positionPercent,
			}
			select {
			case channel <- params:
			default:
				fmt.Printf("channel full for %v - skipping value %v", *id, params)
			}
		}
	}
	channels := handler.PlaybackPositionPercentChangedChannels["PlaybackPositionPercentChanged"]

	params := PlaybackPositionPercentChangedResponseParams{
		PositionPercent: positionPercent,
	}
	select {
	case channels[0] <- params:
	default:
		fmt.Printf("channel full for PlaybackPositionPercentChanged - skipping value %v", params)
	}

}

func ConnectPlaybackPositionPercentChanged(id *string, channelSize uint8) chan PlaybackPositionPercentChangedResponseParams {
	connectId := "PlaybackPositionPercentChanged"
	if id != nil {
		connectId = *id
		channel := make(chan PlaybackPositionPercentChangedResponseParams, channelSize)
		channelList := handler.PlaybackPositionPercentChangedChannels[connectId]
		channelList = append(channelList, channel)
		handler.PlaybackPositionPercentChangedChannels[connectId] = channelList
		return channel
	}
	channel := handler.PlaybackPositionPercentChangedChannels[connectId]
	return channel[0]
}

func EmitSettingsChanged(id *string, value interface{}) {
	if id != nil {
		channelsById := handler.SettingsChangedChannels[*id]
		for _, channel := range channelsById {
			params := SettingsChangedResponseParams{
				Value: value,
			}
			select {
			case channel <- params:
			default:
				fmt.Printf("channel full for %v - skipping value %v", *id, params)
			}
		}
	}
	channels := handler.SettingsChangedChannels["SettingsChanged"]

	params := SettingsChangedResponseParams{
		Value: value,
	}
	select {
	case channels[0] <- params:
	default:
		fmt.Printf("channel full for SettingsChanged - skipping value %v", params)
	}

}

func ConnectSettingsChanged(id *string, channelSize uint8) chan SettingsChangedResponseParams {
	connectId := "SettingsChanged"
	if id != nil {
		connectId = *id
		channel := make(chan SettingsChangedResponseParams, channelSize)
		channelList := handler.SettingsChangedChannels[connectId]
		channelList = append(channelList, channel)
		handler.SettingsChangedChannels[connectId] = channelList
		return channel
	}
	channel := handler.SettingsChangedChannels[connectId]
	return channel[0]
}
