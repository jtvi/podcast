package library

import (
	"gitlab.com/jtvi/podcast/app/library/sourceManager/interfaces"
	"gitlab.com/jtvi/podcast/app/library/sourceManager/itunes"
	"gitlab.com/jtvi/podcast/app/settings"
	"sync"
)

var instance *Controller
var once sync.Once

type Controller struct {
	dataAccess DataInterface
	source     interfaces.SourceInterface
}

func NewController(source interfaces.SourceInterface) *Controller {
	library := new(Controller)
	library.source = source
	return library
}

func GetInstance() *Controller {
	if instance == nil {
		once.Do(func() {
			itunesDataAccess := itunes.NewDataAccess(settings.APPID)
			itunesCtrl := itunes.NewITunesController(itunesDataAccess)
			library := NewController(itunesCtrl)
			instance = library
		})
	}
	return instance
}

func (library *Controller) GetRecommendations() ([]interfaces.PodcastMetaData, error) {
	return library.source.GetRecommendations(100)
}

func (library *Controller) GetCoverArt(id string, url string) (interfaces.FileInterface, error) {
	return library.source.GetCoverArt(id, url)
}
func (library *Controller) Search(id string) ([]interfaces.PodcastMetaData, error) {
	return library.source.Search(id)
}
func (library *Controller) GetPodcast(id string) (interfaces.PodcastInterface, error) {
	return library.source.GetPodcast(id)
}
