package library

import (
	"gitlab.com/jtvi/podcast/app/library/sourceManager/itunes"
)

type DataInterface interface {
	ExecuteGetRequest(urlString string) ([]byte, error)
}

type DataAccess struct {
	itunes *itunes.ITunesController
}

func NewDataAccess(itunes *itunes.ITunesController) *DataAccess {
	d := new(DataAccess)
	d.itunes = itunes
	return d
}
