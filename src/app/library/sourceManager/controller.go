package sourceManager

import (
	"github.com/pkg/errors"
	"gitlab.com/jtvi/podcast/app/library/sourceManager/interfaces"
)

type Controller struct {
	data    DataInterface
	sources map[string]interfaces.SourceInterface
}

func NewController(sources []interfaces.SourceInterface, data DataInterface) *Controller {
	ctrl := new(Controller)
	ctrl.data = data

	sourcesMap := make(map[string]interfaces.SourceInterface, 0)
	for _, source := range sources {
		sourcesMap[source.GetType()] = source
	}
	ctrl.sources = sourcesMap
	return ctrl
}

func (ctrl *Controller) GetRecommendations(count int) (map[string][]interfaces.PodcastMetaData, []error) {
	recommended := make(map[string][]interfaces.PodcastMetaData, 0)
	errors := make([]error, 0)
	for _, source := range ctrl.sources {
		result, err := source.GetRecommendations(count)
		if err != nil {
			errors = append(errors, err)
		} else {
			recommended[source.GetType()] = result
		}
	}
	return recommended, errors
}

func (ctrl *Controller) Search(sourceType string, term string) ([]interfaces.PodcastMetaData, error) {
	source, found := ctrl.sources[sourceType]
	if !found {
		return nil, errors.New("cannot find source")
	}
	return source.Search(term)
}

func (ctrl *Controller) GetPodcast(sourceType string, id string) (interfaces.PodcastInterface, error) {
	source, found := ctrl.sources[sourceType]
	if !found {
		return nil, errors.New("cannot find source")
	}
	return source.GetPodcast(id)
}
