package dataTypes

import (
	"gitlab.com/jtvi/podcast/app/library/sourceManager/interfaces"
)

type Podcast struct {
	Id          string
	Name        string
	FeedUrl     string
	CoverArtUrl string
	Artist      string
	Description string
	SourceType  string
	Episodes    []interfaces.EpisodeInterface
}

func (podcast *Podcast) GetId() string {
	return podcast.Id
}

func (podcast *Podcast) GetName() string {
	return podcast.Name
}

func (podcast *Podcast) GetFeedUrl() string {
	return podcast.FeedUrl
}

func (podcast *Podcast) GetCoverArtUrl() string {
	return podcast.CoverArtUrl
}

func (podcast *Podcast) GetArtist() string {
	return podcast.Artist
}

func (podcast *Podcast) GetDescription() string {
	return podcast.Description
}

func (podcast *Podcast) GetSourceType() string {
	return podcast.SourceType
}

func (podcast *Podcast) GetEpisodes() []interfaces.EpisodeInterface {
	return podcast.Episodes
}
