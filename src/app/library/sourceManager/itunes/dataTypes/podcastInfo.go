package dataTypes

import "gitlab.com/jtvi/podcast/app/library/sourceManager/interfaces"

type PodcastInfo struct {
	Id          string
	Name        string
	CoverArtUrl string
	CoverArt    func() (interfaces.FileInterface, error)
}

func (podcastInfo *PodcastInfo) GetCoverArt() (interfaces.FileInterface, error) {
	return podcastInfo.CoverArt()
}

func (podcastInfo *PodcastInfo) GetId() string {
	return podcastInfo.Id
}

func (podcastInfo *PodcastInfo) GetName() string {
	return podcastInfo.Name
}

func (podcastInfo *PodcastInfo) GetCoverArtUrl() string {
	return podcastInfo.CoverArtUrl
}
