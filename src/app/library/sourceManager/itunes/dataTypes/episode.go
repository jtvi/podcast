package dataTypes

import (
	"time"
)

type Episode struct {
	Title       string
	Url         string
	Duration    string
	Date        *time.Time
	Description string
	Progress    int64
	ID          string
}

func (episode *Episode) GetTitle() string {
	return episode.Title
}

func (episode *Episode) GetUrl() string {
	return episode.Url
}

func (episode *Episode) GetDuration() string {
	return episode.Duration
}

func (episode *Episode) GetDate() *time.Time {
	return episode.Date
}

func (episode *Episode) GetDescription() string {
	return episode.Description
}

func (episode *Episode) GetProgress() int64 {
	return episode.Progress
}

func (episode *Episode) GetID() string {
	return episode.ID
}
