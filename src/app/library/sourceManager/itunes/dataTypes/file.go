package dataTypes

import (
	"path/filepath"
)

type File struct {
	Data     []byte
	FileType string
	FilePath string
}

func (file *File) GetData() []byte {
	return file.Data
}

func (file *File) GetFileType() string {
	return file.FileType
}

func (*File) GetFilePath() string {
	panic("implement me")
}

func NewFileData(filepath string, data []byte) *File {
	fileData := new(File)
	fileData.Data = data
	fileData.FilePath = filepath
	fileData.FileType = getFileTypeFromExtension(filepath)

	return fileData
}

func getFileTypeFromExtension(filePath string) string {
	extension := filepath.Ext(filePath)
	switch extension {
	case ".jpg":
		return "jpeg"
	case ".png":
		return "png"
	}
	panic("File extension not handled")
}
