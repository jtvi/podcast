package dataTypes

//TopJSONSingleEntry describes the root element in the json data.
type TopJSONSingleEntry struct {
	SinglePodcastFeed `json:"feed"`
}

//TopJSONMultipleEntries describes the root element in the json data.
type TopJSONMultipleEntries struct {
	MultiplePodcastFeed `json:"feed"`
}

//SinglePodcastFeed describes the feed element in the json data.
type SinglePodcastFeed struct {
	Entry Entry `json:"entry"`
}

//MultiplePodcastFeed describes the feed element in the json data.
type MultiplePodcastFeed struct {
	Entry []Entry `json:"entry"`
}

//Entry describes the entry element in the json data.
type Entry struct {
	ID     `json:"id"`
	Name   `json:"im:name"`
	Images []Image `json:"im:image"`
}

//ID describes the id element in the json data.
type ID struct {
	IDAttributes `json:"attributes"`
}

//IDAttributes describes the attributes of id element in the json data.
type IDAttributes struct {
	ID string `json:"im:id"`
}

//Name describes the name element in the json data.
type Name struct {
	Label string `json:"label"`
}

//Image describes the image element in the json data.
type Image struct {
	Label string `json:"label"`
}
