package dataTypes

type LookupJSONRoot struct {
	Results []LookupData `json:"results"`
}

type LookupData struct {
	FeedUrl     string `json:"feedUrl"`
	CoverArtUrl string `json:"artworkUrl600"`
}
