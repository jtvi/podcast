package dataTypes

import (
	"strconv"
)

type SearchJSONRoot struct {
	Items []SearchJSONResult `json:"results"`
}

type SearchJSONResult struct {
	Id       int    `json:"collectionId"`
	Name     string `json:"trackName"`
	CoverArt string `json:"artworkUrl100"`
}

func (result *SearchJSONResult) GetName() string {
	return result.Name
}

func (result *SearchJSONResult) GetId() string {
	return strconv.Itoa(result.Id)
}

func (result *SearchJSONResult) GetCoverArtUrl() string {
	return result.CoverArt
}
