package itunes

import (
	"encoding/json"
	"fmt"
	"github.com/kennygrant/sanitize"
	"github.com/mmcdole/gofeed"
	"gitlab.com/jtvi/podcast/app/library/sourceManager/itunes/dataTypes"

	"gitlab.com/jtvi/podcast/app/library/sourceManager/interfaces"
	"gitlab.com/jtvi/podcast/app/logging"
	"gitlab.com/jtvi/podcast/app/translations"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"errors"
)

//ITunesController is business logic handler for itunes module.
type ITunesController struct {
	data DataInterface
}

//NewITunesController creates a new instance of ITunesController business logic.
func NewITunesController(dataInterface DataInterface) *ITunesController {
	ctrl := new(ITunesController)
	ctrl.data = dataInterface
	return ctrl
}

func (ctrl *ITunesController) GetType() string {
	return "itunes"
}

func (ctrl *ITunesController) Search(term string) ([]interfaces.PodcastMetaData, error) {
	term = strings.Replace(term, " ", "+", -1)

	url := fmt.Sprintf("https://itunes.apple.com/search?term=%s&limit=%d", term, 25)
	data, err := ctrl.data.ExecuteGetRequest(url)
	if err != nil {
		return nil, err
	}
	var searchResult = new(dataTypes.SearchJSONRoot)
	err = json.Unmarshal(data, searchResult)
	if err != nil {
		return nil, err
	}
	podcastInfos := make([]interfaces.PodcastMetaData, 0)
	for _, result := range searchResult.Items {
		podcastInfo := new(dataTypes.PodcastInfo)
		podcastInfo.Id = strconv.Itoa(result.Id)
		podcastInfo.Name = result.Name
		podcastInfo.CoverArtUrl = result.CoverArt
		podcastInfo.CoverArt = func() (interfaces.FileInterface, error) {
			return ctrl.GetCoverArt(podcastInfo.Id, podcastInfo.CoverArtUrl)
		}
		podcastInfos = append(podcastInfos, podcastInfo)
	}

	return podcastInfos, nil
}

func (ctrl *ITunesController) GetCoverArt(id string, url string) (interfaces.FileInterface, error) {
	var data = make([]byte, 0)
	fileSize := filepath.Base(url)
	filename := fmt.Sprintf("%s_%s", id, fileSize)
	file, err := ctrl.data.LoadFile(filename, true)
	if err == nil {
		data = file
	}
	file, err = ctrl.data.LoadFile(filename, false)
	if err == nil {
		data = file
	}
	webData, err := ctrl.data.ExecuteGetRequest(url)
	if err != nil {
		return nil, err
	}
	err = ctrl.data.SaveCoverArt(filename, false, webData)
	if err != nil {
		return nil, err
	}
	data = webData

	return dataTypes.NewFileData(filename, data), nil
}

func (ctrl *ITunesController) GetPodcast(id string) (interfaces.PodcastInterface, error) {
	lookupData, err := ctrl.getLookupData(id)
	if err != nil {
		return nil, err
	}
	fp := gofeed.NewParser()
	podcastBytes, err := ctrl.data.ExecuteGetRequest(lookupData.FeedUrl)
	if err != nil {
		return nil, err
	}

	feed, err := fp.ParseString(string(podcastBytes))
	if err != nil {
		return nil, err
	}
	podcast := new(dataTypes.Podcast)
	podcast.Id = id
	podcast.Name = sanitize.HTML(feed.Title)
	if feed.Author != nil {
		podcast.Artist = sanitize.HTML(feed.Author.Name)
	}
	podcast.Description = sanitize.HTML(feed.Description)
	podcast.CoverArtUrl = lookupData.CoverArtUrl
	episodes := make([]interfaces.EpisodeInterface, 0)

	for i := 0; i < len(feed.Items); i++ {
		if len(feed.Items[i].Enclosures) != 1 {
			logging.LogError(errors.New(fmt.Sprintf("BUG: Parser cannot find a stream : %s", feed.Items[i].Title)))
			continue
		}
		duration := translations.T("unknown")

		if feed.Items[i].ITunesExt != nil {
			duration = feed.Items[i].ITunesExt.Duration
			if !strings.Contains(duration, ":") {
				seconds, err := strconv.ParseInt(duration, 0, 64)
				if err == nil {
					durationTime := time.Unix(0, seconds*1000000000)
					duration = durationTime.UTC().Format("15:04:02")
				}
			}
		}

		episode := dataTypes.Episode{
			Title:       sanitize.HTML(feed.Items[i].Title),
			Url:         feed.Items[i].Enclosures[0].URL,
			Date:        feed.Items[i].PublishedParsed,
			Description: sanitize.HTML(feed.Items[i].Description),
			Duration:    duration,
			ID:          feed.Items[i].GUID,
		}
		episodes = append(episodes, &episode)
	}
	podcast.Episodes = episodes
	return podcast, nil
}

func (ctrl *ITunesController) GetRecommendations(count int) ([]interfaces.PodcastMetaData, error) {
	url := fmt.Sprintf("http://itunes.apple.com/%s/rss/toppodcasts/limit=%d/json", "fi", count)
	data, err := ctrl.data.ExecuteGetRequest(url)
	if err != nil {
		return nil, err
	}
	var entries = make([]dataTypes.Entry, 0)
	var jsonRoot = new(dataTypes.TopJSONMultipleEntries)
	err = json.Unmarshal(data, jsonRoot)

	if err == nil {
		entries = jsonRoot.MultiplePodcastFeed.Entry
	} else {
		var singleRoot = new(dataTypes.TopJSONSingleEntry)
		err = json.Unmarshal(data, singleRoot)
		if err != nil {
			return nil, err
		}
		entries = append(entries, singleRoot.SinglePodcastFeed.Entry)
	}

	podcastInfos := make([]interfaces.PodcastMetaData, 0)

	for _, entry := range entries {
		id := entry.ID.IDAttributes.ID
		url := entry.Images[2].Label
		podcastInfo := dataTypes.PodcastInfo{
			Id:          id,
			CoverArtUrl: url,
			Name:        entry.Name.Label,
			CoverArt: func() (interfaces.FileInterface, error) {
				return ctrl.GetCoverArt(id, url)
			},
		}
		podcastInfos = append(podcastInfos, &podcastInfo)
	}
	return podcastInfos, nil
}

func (ctrl *ITunesController) getLookupData(id string) (*dataTypes.LookupData, error) {
	url := fmt.Sprintf("https://itunes.apple.com/lookup?id=%s", id)
	appleLookupData, err := ctrl.data.ExecuteGetRequest(url)
	if err != nil {
		return nil, err
	}

	jsonRoot := new(dataTypes.LookupJSONRoot)
	err = json.Unmarshal(appleLookupData, &jsonRoot)
	if err != nil {
		return nil, err
	}
	lookupData := jsonRoot.Results[0]

	return &lookupData, nil
}

func (ctrl *ITunesController) getGenre(genreID string) (*[]dataTypes.PodcastInfo, error) {
	return nil, errors.New("NotImplemented")
	//url := fmt.Sprintf("https://itunes.apple.com/search?term=podcast&genreId=%s&limit=200", genreID)
	////data, err := ctrl.data.ExecuteGetRequest(url)
	//if err != nil {
	//	log.Fatal("Cannot fetch itunes top 100")
	//}
	//return nil,nil
}
