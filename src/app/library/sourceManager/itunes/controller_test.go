package itunes

import (
	"testing"
)

func TestITunes_GetTop100_Return2Podcasts(t *testing.T) {

	//mockCtrl := gomock.NewITunesController(t)
	//defer mockCtrl.Finish()
	//mockDataAccess := mocks.NewMockIDataAccess(mockCtrl)
	//
	//dummyDataBytes := []byte(dummyData2Podcasts)
	//mockDataAccess.EXPECT().ExecuteGetRequest("http://itunes.apple.com/fi/rss/toppodcasts/limit=100/json").Return(dummyDataBytes, nil).Times(1)
	//
	//target := NewITunesController("testApp", mockDataAccess)
	//result, err := target.GetTopPodcasts()
	//assert.Nil(t, err)
	//
	//assert.Equal(t, 2, len(result), "Should return 2 podcasts")
	//
	////Asserting first podcast_app.
	//first := (result)[0]
	//
	//assert.Equal(t, "1357695290", first.Id)
	//assert.Equal(t, "Death in Ice Valley", first.Name)
	//assert.Equal(t, "https://is2-ssl.mzstatic.com/image/thumb/Podcasts118/v4/89/bb/53/89bb53f2-b257-ced3-3cfc-1ae6145bd89d/mza_5204228570590316124.jpg/170x170bb-85.png",
	//	first.CoverArtUrl)
	//
	//// Asserting second podcast_app.
	//second := (result)[1]
	//assert.Equal(t, "1200361736", second.Id)
	//assert.Equal(t, "The Daily", second.Name)
	//assert.Equal(t, "https://is4-ssl.mzstatic.com/image/thumb/Podcasts122/v4/37/9d/b6/379db6f4-629b-7869-ab40-531fa56532ce/mza_6120789080010223365.jpeg/170x170bb-85.png",
	//	second.CoverArtUrl)
}

func TestITunes_GetTop100_Return1Podcast(t *testing.T) {

	//mockCtrl := gomock.NewITunesController(t)
	//defer mockCtrl.Finish()
	//mockDataAccess := mocks.NewMockIDataAccess(mockCtrl)
	//
	//dummyDataBytes := []byte(dummyData1Podcast)
	//mockDataAccess.EXPECT().ExecuteGetRequest("http://itunes.apple.com/fi/rss/toppodcasts/limit=100/json").Return(dummyDataBytes, nil).Times(1)
	//
	//target := NewITunesController("testApp", mockDataAccess)
	//result, err := target.GetTopPodcasts()
	//assert.Nil(t, err)
	//
	//assert.Equal(t, 1, len(result), "Should return one podcast_app")
	//
	////Asserting first podcast_app.
	//first := (result)[0]
	//assert.Equal(t, "1357695290", first.Id)
	//assert.Equal(t, "Death in Ice Valley", first.Name)
	//assert.Equal(t, "https://is2-ssl.mzstatic.com/image/thumb/Podcasts118/v4/89/bb/53/89bb53f2-b257-ced3-3cfc-1ae6145bd89d/mza_5204228570590316124.jpg/170x170bb-85.png",
	//	first.CoverArtUrl)
}

var dummyData1Podcast = `{
	"feed":{
		"author":{
			"name":{
				"label":"iTunes Store"
			},
			"uri":{
				"label":"http://www.apple.com/itunes/"
			}
		},
		"entry":{
			"im:name":{
				"label":"Death in Ice Valley"
			},
			"im:image":[
				{"label":"https://is1-ssl.mzstatic.com/image/thumb/Podcasts118/v4/89/bb/53/89bb53f2-b257-ced3-3cfc-1ae6145bd89d/mza_5204228570590316124.jpg/55x55bb-85.png", "attributes":{"height":"55"}},
				{"label":"https://is2-ssl.mzstatic.com/image/thumb/Podcasts118/v4/89/bb/53/89bb53f2-b257-ced3-3cfc-1ae6145bd89d/mza_5204228570590316124.jpg/60x60bb-85.png", "attributes":{"height":"60"}},
				{"label":"https://is2-ssl.mzstatic.com/image/thumb/Podcasts118/v4/89/bb/53/89bb53f2-b257-ced3-3cfc-1ae6145bd89d/mza_5204228570590316124.jpg/170x170bb-85.png", "attributes":{"height":"170"}}],
				"summary":{
					"label":"An unidentified body. Who was she? Why hasn’t she been missed? A BBC World Service and NRK original podcast_app, investigating a mystery unsolved for almost half a century."
				},
				"im:price":{
					"label":"Get",
					"attributes":{
						"amount":"0",
						"currency":"USD"
					}
				},
				"im:contentType":{
					"attributes":{
						"term":"Podcast",
						"label":"Podcast"
					}
				},
				"rights":{
					"label":"© (C) BBC 2018"
				},
				"title":{
					"label":"Death in Ice Valley - BBC World Service"
				},
				"link":{
					"attributes":{
						"rel":"alternate",
						"type":"text/html",
						"href":"https://itunes.apple.com/us/podcast_app/death-in-ice-valley/id1357695290?mt=2&uo=2"
					}
				},
				"id":{
					"label":"https://itunes.apple.com/us/podcast_app/death-in-ice-valley/id1357695290?mt=2&uo=2",
					"attributes":{
						"im:id":"1357695290"
					}
				},
				"im:artist":{
					"label":"BBC World Service",
					"attributes":{
						"href":"https://itunes.apple.com/us/artist/bbc/121676617?mt=2&uo=2"
					}
				},
				"category":{
					"attributes":{
						"im:id":"1311",
						"term":"News & Politics",
						"scheme":"https://itunes.apple.com/us/genre/podcasts-news-politics/id1311?mt=2&uo=2",
						"label":"News & Politics"
					}
				},
				"im:releaseDate":{
					"label":"2018-04-15T18:00:00-07:00",
					"attributes":{
						"label":"April 15, 2018"
					}
				}
			},
			"updated":{
				"label":"2018-04-18T03:56:30-07:00"
			},
			"rights":{
				"label":"Copyright 2008 Apple Inc."
			},
			"title":{
				"label":"iTunes Store: Top Podcasts"
			},
			"icon":{
				"label":"http://itunes.apple.com/favicon.ico"
			},
			"link":[
				{"attributes":{"rel":"alternate", "type":"text/html", "href":"https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewTop?cc=us&id=1&popId=3"}},
				{"attributes":{"rel":"self", "href":"http://itunes.apple.com/us/rss/toppodcasts/limit=1/json"}}],
			"id":{
				"label":"http://itunes.apple.com/us/rss/toppodcasts/limit=1/json"
			}
		}
	}`

var dummyData2Podcasts = ` {
		"feed":{
			"author":{
				"name":{
					"label":"iTunes Store"
				},
				"uri":{
					"label":"http://www.apple.com/itunes/"
				}
			},
			"entry":[
				{
					"im:name":{
						"label":"Death in Ice Valley"
					},
					"im:image":[
						{"label":"https://is1-ssl.mzstatic.com/image/thumb/Podcasts118/v4/89/bb/53/89bb53f2-b257-ced3-3cfc-1ae6145bd89d/mza_5204228570590316124.jpg/55x55bb-85.png", "attributes":{"height":"55"}},
						{"label":"https://is2-ssl.mzstatic.com/image/thumb/Podcasts118/v4/89/bb/53/89bb53f2-b257-ced3-3cfc-1ae6145bd89d/mza_5204228570590316124.jpg/60x60bb-85.png", "attributes":{"height":"60"}},
						{"label":"https://is2-ssl.mzstatic.com/image/thumb/Podcasts118/v4/89/bb/53/89bb53f2-b257-ced3-3cfc-1ae6145bd89d/mza_5204228570590316124.jpg/170x170bb-85.png", "attributes":{"height":"170"}}
					],
					"summary":{
						"label":"An unidentified body. Who was she? Why hasn’t she been missed? A BBC World Service and NRK original podcast_app, investigating a mystery unsolved for almost half a century."
					},
					"im:price":
						{
							"label":"Get",
							"attributes":{
								"amount":"0",
								"currency":"USD"
							}
						},
						"im:contentType":{
							"attributes":{
								"term":"Podcast",
								"label":"Podcast"
							}
						},
						"rights":{
							"label":"© (C) BBC 2018"
						},
						"title":{
							"label":"Death in Ice Valley - BBC World Service"
						},
						"link":{
							"attributes":{
								"rel":"alternate", "type":"text/html",
								"href":"https://itunes.apple.com/us/podcast_app/death-in-ice-valley/id1357695290?mt=2&uo=2"
							}
						},
						"id":{
							"label":"https://itunes.apple.com/us/podcast_app/death-in-ice-valley/id1357695290?mt=2&uo=2",
							"attributes":{
								"im:id":"1357695290"
							}
						},
						"im:artist":{
							"label":"BBC World Service",
							"attributes":{
								"href":"https://itunes.apple.com/us/artist/bbc/121676617?mt=2&uo=2"
							}
						},
						"category":{
							"attributes":{
								"im:id":"1311",
								"term":"News & Politics",
								"scheme":"https://itunes.apple.com/us/genre/podcasts-news-politics/id1311?mt=2&uo=2",
								"label":"News & Politics"
							}
						},
						"im:releaseDate":{
							"label":"2018-04-15T18:00:00-07:00",
							"attributes":{"label":"April 15, 2018"
						}
					}
				},
				{
					"im:name":{
						"label":"The Daily"
					},
					"im:image":[
						{"label":"https://is1-ssl.mzstatic.com/image/thumb/Podcasts122/v4/37/9d/b6/379db6f4-629b-7869-ab40-531fa56532ce/mza_6120789080010223365.jpeg/55x55bb-85.png", "attributes":{"height":"55"}},
						{"label":"https://is2-ssl.mzstatic.com/image/thumb/Podcasts122/v4/37/9d/b6/379db6f4-629b-7869-ab40-531fa56532ce/mza_6120789080010223365.jpeg/60x60bb-85.png", "attributes":{"height":"60"}},
						{"label":"https://is4-ssl.mzstatic.com/image/thumb/Podcasts122/v4/37/9d/b6/379db6f4-629b-7869-ab40-531fa56532ce/mza_6120789080010223365.jpeg/170x170bb-85.png", "attributes":{"height":"170"}}
					],
					"summary":{
						"label":"This is what the news should sound like. The biggest stories of our time, told by the best journalists in the world. Hosted by Michael Barbaro. Twenty minutes a day, five days a week, ready by 6 a.m."
					},
					"im:price":{
						"label":"Get",
						"attributes":{
							"amount":"0", "currency":"USD"
						}
					},
					"im:contentType":{
						"attributes":{
							"term":"Podcast",
							"label":"Podcast"
						}
					},
					"rights":{
						"label":"© 2017"
					},
					"title":{
						"label":"The Daily - The NewITunesController York Times"
					},
					"link":{
						"attributes":{
							"rel":"alternate",
							"type":"text/html",
							"href":"https://itunes.apple.com/us/podcast_app/the-daily/id1200361736?mt=2&uo=2"
						}
					},
					"id":{
						"label":"https://itunes.apple.com/us/podcast_app/the-daily/id1200361736?mt=2&uo=2",
						"attributes":{
							"im:id":"1200361736"
						}
					},
					"im:artist":{
						"label":"The NewITunesController York Times",
						"attributes":{
							"href":"https://itunes.apple.com/us/artist/the-new-york-times/121664449?mt=2&uo=2"
						}
					},
					"category":{
						"attributes":{
							"im:id":"1311",
							"term":"News & Politics",
							"scheme":"https://itunes.apple.com/us/genre/podcasts-news-politics/id1311?mt=2&uo=2",
							"label":"News & Politics"
						}
					},
					"im:releaseDate":{
						"label":"2018-04-18T02:54:00-07:00",
						"attributes":{
							"label":"April 18, 2018"
						}
					}
				}],
			 "updated":{
				 "label":"2018-04-19T00:24:07-07:00"
				 },
				"rights":{
					"label":"Copyright 2008 Apple Inc."
					},
					"title":{
						"label":"iTunes Store: Top Podcasts"
					},
					"icon":{
						"label":"http://itunes.apple.com/favicon.ico"
					},
					"link":[
						{
							"attributes":{
								"rel":"alternate",
								"type":"text/html",
								"href":"https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewTop?cc=us&id=1&popId=3"
							}
						},
						{
							"attributes":{
								"rel":"self",
								"href":"http://itunes.apple.com/us/rss/toppodcasts/limit=2/json"
							}
						}
					],
				"id":{
					"label":"http://itunes.apple.com/us/rss/toppodcasts/limit=2/json"
				}
			}
		}`
