package itunes

import (
	"gitlab.com/jtvi/podcast/app/filecache"
	"gitlab.com/jtvi/podcast/app/utils"
)

const fileCacheId = "coverArt"

//DataInterface describes data access interface needed by the itunes business logic.
type DataInterface interface {
	LoadFile(filename string, persist bool) ([]byte, error)
	SaveCoverArt(filename string, persist bool, data []byte) error
	ExecuteGetRequest(url string) ([]byte, error)
}

//DataAccess is the implementation of DataInterface interface.
type DataAccess struct {
	appName   string
	fileCache filecache.Controller
}

//NewDataAccess creates a new data access implementation.
func NewDataAccess(appName string) *DataAccess {
	da := DataAccess{
		appName:   appName,
		fileCache: *filecache.NewController(appName, fileCacheId, filecache.DataAccessNew()),
	}

	return &da
}

//FindFile finds a given file from the disk.
func (da *DataAccess) LoadFile(filename string, persist bool) ([]byte, error) {
	return da.fileCache.LoadFile(filename, persist)
}

//SaveCoverArt cover art data to the disk.
func (da *DataAccess) SaveCoverArt(filename string, persist bool, data []byte) error {
	return da.fileCache.SaveFile(filename, persist, data)
}

//ExecuteGetRequest executes a GET-request from a  given url.
func (da *DataAccess) ExecuteGetRequest(urlString string) ([]byte, error) {
	return utils.ExecuteGetRequest(urlString)
}
