package interfaces

//type FileType string
//
//const (
//	ImageTypeJpg FileType = "jpeg"
//	ImageTypePng FileType = "png"
//)

type FileInterface interface {
	GetData() []byte
	GetFileType() string
	GetFilePath() string
}
