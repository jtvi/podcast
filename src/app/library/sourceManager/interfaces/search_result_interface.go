package interfaces

type PodcastMetaData interface {
	GetName() string
	GetCoverArtUrl() string
	GetId() string
	GetCoverArt() (FileInterface, error)
}
