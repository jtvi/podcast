package interfaces

type SourceInterface interface {
	GetType() string
	Search(string) ([]PodcastMetaData, error)
	GetCoverArt(id string, url string) (FileInterface, error)
	GetPodcast(id string) (PodcastInterface, error)
	GetRecommendations(i int) ([]PodcastMetaData, error)
}
