package interfaces

import "time"

type EpisodeInterface interface {
	GetTitle() string
	GetUrl() string
	GetDuration() string
	GetDate() *time.Time
	GetDescription() string
	GetProgress() int64
	GetID() string
}
