package interfaces

type PodcastInterface interface {
	GetId() string
	GetName() string
	GetFeedUrl() string
	GetCoverArtUrl() string
	GetArtist() string
	GetDescription() string
	GetSourceType() string
	GetEpisodes() []EpisodeInterface
}
