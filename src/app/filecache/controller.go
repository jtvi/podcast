package filecache

import (
	"path"

	"github.com/kyoh86/xdg"
	"github.com/pkg/errors"
	"gitlab.com/jtvi/podcast/app/logging"
)

type Controller struct {
	appName    string
	sourceID   string
	cacheDir   string
	dataDir    string
	dataAccess IDataAccess
}

func NewController(appName string, sourceID string, dataAccess IDataAccess) *Controller {
	ctrl := new(Controller)
	ctrl.dataAccess = dataAccess

	cacheDir := path.Join(xdg.CacheHome(), appName, sourceID)
	ctrl.createDirIfNotExist(cacheDir)

	dataDir := path.Join(xdg.DataHome(), appName, sourceID)
	ctrl.createDirIfNotExist(dataDir)
	ctrl.appName = appName
	ctrl.sourceID = sourceID
	ctrl.cacheDir = cacheDir
	ctrl.dataDir = dataDir

	return ctrl
}

func (ctrl *Controller) FindFile(id string) (string, error) {
	fullPath, err := xdg.FindCacheFile(ctrl.appName, ctrl.sourceID, id)
	if err == nil {
		return fullPath, nil
	}

	return xdg.FindDataFile(ctrl.appName, ctrl.sourceID, id)
}

func (ctrl *Controller) SaveFile(filename string, persist bool, data []byte) error {
	fullPath := ctrl.createFullPath(filename, persist)
	return ctrl.dataAccess.SaveFile(fullPath, data)
}

func (ctrl *Controller) LoadFile(filename string, persist bool) ([]byte, error) {
	fullPath := ctrl.createFullPath(filename, persist)
	return ctrl.dataAccess.LoadFile(fullPath)
}

func (ctrl *Controller) createDirIfNotExist(path string) {
	fileinfo, err := ctrl.dataAccess.GetFileInfo(path)
	if err != nil {
		err = ctrl.dataAccess.CreateDir(path)
		if err != nil {
			err := errors.New("Cannot create directory for cover art")
			logging.LogCritical(err)
			return
		}
		return
	}

	if !fileinfo.IsDir() {
		err := errors.New("directory is a file")
		logging.LogCritical(err)
	}
}

func (ctrl *Controller) createFullPath(filename string, persist bool) string {
	var fullPath string
	if persist {
		fullPath = path.Join(ctrl.dataDir, filename)
	} else {
		fullPath = path.Join(ctrl.cacheDir, filename)
	}
	return fullPath
}
