package filecache

import (
	"io/ioutil"
	"os"
)

type IDataAccess interface {
	SaveFile(filename string, data []byte) error
	LoadFile(filename string) ([]byte, error)
	GetFileInfo(filename string) (os.FileInfo, error)
	CreateDir(path string) error
}

type DataAccess struct {
}

func (da *DataAccess) CreateDir(path string) error {
	return os.MkdirAll(path, os.ModePerm)
}

func DataAccessNew() *DataAccess {
	return new(DataAccess)
}

func (da *DataAccess) SaveFile(filename string, data []byte) error {
	return ioutil.WriteFile(filename, data, 0644)
}

func (da *DataAccess) LoadFile(filename string) ([]byte, error) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return data, nil
}
func (da *DataAccess) GetFileInfo(filepath string) (os.FileInfo, error) {
	return os.Stat(filepath)
}
