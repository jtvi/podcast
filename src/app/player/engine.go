package player

import (
	"github.com/gotk3/gotk3/glib"
	"github.com/spreadspace/go-gstreamer"
	"gitlab.com/jtvi/podcast/app/library/sourceManager/interfaces"
	"gitlab.com/jtvi/podcast/app/logging"
	"gitlab.com/jtvi/podcast/app/settings"
	"gitlab.com/jtvi/podcast/app/signals"
)

type engine struct {
	playBin        *gst.Element
	isPlaying      bool
	isSeeking      bool
	currentEpisode interfaces.EpisodeInterface
}

func newEngine() (*engine, error) {
	engine := new(engine)
	gst.Init(nil)
	playBin, err := gst.ElementFactoryMake("playbin", "play")
	if err != nil {
		return nil, err
	}
	engine.isPlaying = false
	engine.playBin = playBin
	signals.EmitVolumeChanged(nil, settings.GetFloat("volume"))
	//signals.ConnectSetVolume(nil, func(value float64) {
	//	engine.SetVolume(value)
	//})
	//
	//signals.ConnectSeek(nil, func(value float64) {
	//	engine.playBin.SeekSimple(gst.FORMAT_TIME, gst.SEEK_FLAG_FLUSH, int64(value))
	//})

	return engine, nil
}

func (engine *engine) playNew(episode interfaces.EpisodeInterface) {
	engine.isPlaying = false
	engine.playBin.SetState(gst.STATE_NULL)
	err := engine.playBin.Set("uri", episode.GetUrl())
	logging.LogCritical(err)
	engine.playBin.SetState(gst.STATE_PLAYING)
	engine.currentEpisode = episode
	engine.isPlaying = true
	_, err = glib.TimeoutAdd(500, func(id string) bool {
		if id != engine.currentEpisode.GetID() {
			return false
		}
		positionTime, _ := engine.playBin.QueryPosition(gst.FORMAT_TIME)
		positionPercent, _ := engine.playBin.QueryPosition(gst.FORMAT_PERCENT)
		durationTime, _ := engine.playBin.QueryDuration(gst.FORMAT_TIME)
		signals.EmitPlaybackPositionPercentChanged(&id, positionPercent)
		signals.EmitPlaybackPositionChanged(nil, positionTime, durationTime)

		return true
	}, engine.currentEpisode.GetID())
	logging.LogCritical(err)
}

func (engine *engine) SetVolume(value float64) {
	volume, err := engine.playBin.GetProperty("volume")
	logging.LogCritical(err)
	volume = volume.(float64)
	if volume == value {
		return
	}
	err = engine.playBin.Set("volume", value)
	logging.LogCritical(err)
	settings.SetFloat("volume", value)
	signals.EmitVolumeChanged(nil, value)
}

func (engine *engine) togglePlay() {
	state, _, _ := engine.playBin.GetState(1000)
	if state == gst.STATE_PLAYING {
		engine.playBin.SetState(gst.STATE_PAUSED)
		engine.isPlaying = false
	} else {
		engine.playBin.SetState(gst.STATE_PLAYING)
		engine.isPlaying = true
	}
}
