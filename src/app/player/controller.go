package player

import (
	"gitlab.com/jtvi/podcast/app/dataTypes"
	"gitlab.com/jtvi/podcast/app/library/sourceManager/interfaces"
)

type ControllerInterface interface {
	PlayAll([]interfaces.EpisodeInterface)
	TogglePlay()
	PlayNext()
	PlayPrevious()
	GetPlaybackChangedChannel() chan *dataTypes.Status
}

type Controller struct {
	dataAccess             DataInterface
	history                []interfaces.EpisodeInterface
	currentIndex           int
	engine                 *engine
	playbackChangedChannel chan *dataTypes.Status
}

func NewController(dataAccess DataInterface) (*Controller, error) {
	ctrl := new(Controller)
	ctrl.dataAccess = dataAccess
	ctrl.history = make([]interfaces.EpisodeInterface, 0)
	ctrl.currentIndex = -1
	engine, err := newEngine()
	if err != nil {
		return nil, err
	}
	ctrl.engine = engine

	return ctrl, nil
}

func (ctrl *Controller) PlayAll(episodes []interfaces.EpisodeInterface) {
	if len(episodes) == 0 {
		return
	}
	indexBeforeAppend := len(ctrl.history)
	for i := len(episodes) - 1; i >= 0; i-- {
		ctrl.history = append(ctrl.history, episodes[i])
	}

	if ctrl.currentIndex == -1 {
		ctrl.currentIndex = 0
	} else {
		ctrl.currentIndex = indexBeforeAppend
	}

	ctrl.engine.playNew(ctrl.history[ctrl.currentIndex])
	ctrl.SendPlaybackChangedEvent()
}

func (ctrl *Controller) GetPlaybackChangedChannel() chan *dataTypes.Status {
	return ctrl.playbackChangedChannel
}

func (ctrl *Controller) TogglePlay() {
	ctrl.engine.togglePlay()
	ctrl.SendPlaybackChangedEvent()
}

func (ctrl *Controller) PlayNext() {
	nextIndex := ctrl.currentIndex + 1
	ctrl.currentIndex = nextIndex
	ctrl.engine.playNew(ctrl.history[ctrl.currentIndex])
	ctrl.SendPlaybackChangedEvent()
}

func (ctrl *Controller) PlayPrevious() {
	previousIndex := ctrl.currentIndex - 1
	ctrl.currentIndex = previousIndex
	ctrl.engine.playNew(ctrl.history[ctrl.currentIndex])
	ctrl.SendPlaybackChangedEvent()
}

func (ctrl *Controller) SendPlaybackChangedEvent() {
	historyLength := len(ctrl.history)
	status := new(dataTypes.Status)
	status.HasNext = ctrl.currentIndex < historyLength-1
	status.HasPrevious = ctrl.currentIndex > 0
	status.CurrentEpisode = ctrl.history[ctrl.currentIndex]
	status.IsPlaying = ctrl.engine.isPlaying
	status.CanPlay = ctrl.engine.currentEpisode != nil

	ctrl.playbackChangedChannel <- status
}
