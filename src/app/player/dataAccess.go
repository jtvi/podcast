package player

import (
	"gitlab.com/jtvi/podcast/app/utils"
)

type DataInterface interface {
	ExecuteGetRequest(urlString string) ([]byte, error)
}

type DataAccess struct {
}

func NewDataAccess() *DataAccess {
	return &DataAccess{}
}

func (dataAccess *DataAccess) ExecuteGetRequest(urlString string) ([]byte, error) {
	return utils.ExecuteGetRequest(urlString)
}
