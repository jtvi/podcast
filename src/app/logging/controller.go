package logging

import (
	log "github.com/sirupsen/logrus"
	"os"
)

func init() {
	log.SetReportCaller(false)
	log.SetOutput(os.Stdout)
}

func LogDebug(message string) {
	log.Debug(message)
}

func LogInfo(message string) {
	log.Info(message)
}

func LogWarning(message string) {
	log.Warning(message)
}

func LogError(err error) {
	if err == nil {
		return
	}
	log.Error(err)
}

func LogCritical(err error) {
	if err == nil {
		return
	}
	log.Fatal(err)
}
