package translations

import (
	"github.com/leonelquinteros/gotext"
	"os"
	"path"
	"runtime"
)

func init() {
	_, localizationFolder, _, _ := runtime.Caller(1)
	localizationFolder = path.Dir(localizationFolder)
	localizationFolder = path.Join(localizationFolder, "po")
	gotext.Configure(localizationFolder, getLocale(), "default")
}

func T(text string) string {
	return gotext.Get(text)
}

func getLocale() string {
	locale := os.Getenv("LC_ALL")
	if locale == "" {
		locale = os.Getenv("LANG")
	}
	if locale == "" {
		locale = "en"
	}
	return locale
}
