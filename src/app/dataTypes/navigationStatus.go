package dataTypes

type NavigationStatus struct {
	CanNavigateBack    bool
	CanNavigateForward bool
}
