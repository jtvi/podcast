package dataTypes

type ImageSize struct {
	Width  int
	Height int
}
