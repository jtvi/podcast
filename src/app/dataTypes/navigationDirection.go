package dataTypes

type NavigationDirection string

const (
	DirectionNext     NavigationDirection = "next"
	DirectionPrevious NavigationDirection = "previous"
)
