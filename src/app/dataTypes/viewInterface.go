package dataTypes

import (
	"github.com/gotk3/gotk3/gtk"
)

type ViewInterface interface {
	gtk.IWidget
	GetID() string
	GetViewType() string
}
