package dataTypes

import "gitlab.com/jtvi/podcast/app/library/sourceManager/interfaces"

type Status struct {
	CurrentEpisode            interfaces.EpisodeInterface
	IsPlaying                 bool
	CanPlay                   bool
	HasNext                   bool
	HasPrevious               bool
	CurrentlyPlayingEventName string
}
