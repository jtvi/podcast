package app

import (
	"fmt"
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/jtvi/podcast/app/library"
	"gitlab.com/jtvi/podcast/app/library/sourceManager/itunes"
	"gitlab.com/jtvi/podcast/app/logging"
	"gitlab.com/jtvi/podcast/app/player"
	"gitlab.com/jtvi/podcast/app/settings"
	"gitlab.com/jtvi/podcast/app/ui"
	"os"
)

type App struct {
	gtkApp *gtk.Application
}

func NewApp() *App {
	logging.LogInfo("starting podcast")
	app := new(App)
	player := createPlayer()
	itunesSource := itunes.NewITunesController(itunes.NewDataAccess(settings.APPID))
	podcastLibrary := library.NewController(itunesSource)
	mainWindowController := ui.NewMainWindowController(podcastLibrary, player)
	app.gtkApp, _ = gtk.ApplicationNew(settings.APPID, glib.APPLICATION_FLAGS_NONE)
	_, _ = app.gtkApp.Connect("activate", func(gtkApplication *gtk.Application) {
		window := ui.NewMainWindow(gtkApplication, mainWindowController)
		window.ShowAll()
	})
	return app
}

func createPlayer() player.ControllerInterface {
	playerCtrl, err := player.NewController(player.NewDataAccess())
	if err != nil {
		logging.LogCritical(fmt.Errorf("creating new player failed: %v", err))
	}

	//signals.ConnectPlayAll(nil, func(podcastId string) {
	//	//TODO: implementation
	//})
	//
	//signals.ConnectPlayNext(nil, func() {
	//	playerCtrl.PlayNext()
	//})
	//
	//signals.ConnectPlayPrevious(nil, func() {
	//	playerCtrl.PlayPrevious()
	//})

	return playerCtrl
}

func (app *App) Run() {
	app.gtkApp.Run(os.Args)
}
