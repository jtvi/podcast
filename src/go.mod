module gitlab.com/jtvi/podcast

require (
	github.com/PuerkitoBio/goquery v1.5.0 // indirect
	github.com/golang/mock v1.2.0
	github.com/gotk3/gotk3 v0.0.0-20181125224807-4154577b870f
	github.com/kennygrant/sanitize v1.2.4
	github.com/kyoh86/xdg v0.0.0-20180513042900-fcdc7e7a466b
	github.com/leonelquinteros/gotext v1.4.0
	github.com/marcsauter/single v0.0.0-20181104081128-f8bf46f26ec0
	github.com/mmcdole/gofeed v1.0.0-beta2
	github.com/mmcdole/goxpp v0.0.0-20181012175147-0068e33feabf // indirect
	github.com/nu7hatch/gouuid v0.0.0-20131221200532-179d4d0c4d8d
	github.com/pkg/errors v0.8.0
	github.com/sirupsen/logrus v1.2.0
	github.com/spreadspace/go-gstreamer v0.0.0-20161229210907-679bfc729cef
	golang.org/x/net v0.0.0-20181207154023-610586996380 // indirect
	golang.org/x/text v0.3.0 // indirect
)
