package main

import (
	"fmt"
	"github.com/marcsauter/single"
	"gitlab.com/jtvi/podcast/app"
	"gitlab.com/jtvi/podcast/app/logging"
	"gitlab.com/jtvi/podcast/app/settings"
)

func main() {
	podcastApp := app.NewApp() //getAppLock()

	//lock := getAppLock()
	//TODO: Change lock file location on disk.
	//defer lock.TryUnlock()

	podcastApp.Run()
}

func _() (lock *single.Single) {
	lock = single.New(settings.APPID)

	if err := lock.CheckLock(); err != nil && err == single.ErrAlreadyRunning {
		logging.LogWarning("Application is already running.")
	} else if err != nil {
		logging.LogCritical(fmt.Errorf("Failed to acquire exclusive app lock: %v", err))
	}
	return lock
}
