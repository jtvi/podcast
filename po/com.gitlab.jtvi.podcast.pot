# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid   ""
msgstr  "Project-Id-Version: com.gitlab.jtvi.podcast\n"
        "Report-Msgid-Bugs-To: EMAIL\n"
        "POT-Creation-Date: 2018-11-29 19:56+0200\n"
        "PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
        "Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
        "Language-Team: LANGUAGE <LL@li.org>\n"
        "Language: \n"
        "MIME-Version: 1.0\n"
        "Content-Type: text/plain; charset=CHARSET\n"
        "Content-Transfer-Encoding: 8bit\n"

#: /home/jtvi/Projects/Go/src/gitlab.com/jtvi/podcast/podcast_app/library/sources/itunes/itunes.go:117
msgid   "unknown"
msgstr  ""

#: /home/jtvi/Projects/Go/src/gitlab.com/jtvi/podcast/podcast_app/ui/views/top_toolbar.go:51
msgid   "iTunes Store"
msgstr  ""

#: /home/jtvi/Projects/Go/src/gitlab.com/jtvi/podcast/podcast_app/ui/widgets/episode_grid.go:70
msgid   "dateformat"
msgstr  ""

#: /home/jtvi/Projects/Go/src/gitlab.com/jtvi/podcast/podcast_app/gtk_async_loader/loading_view.go:19
msgid   "Loading..."
msgstr  ""

#: /home/jtvi/Projects/Go/src/gitlab.com/jtvi/podcast/podcast_app/gtk_async_loader/error_view.go:37
msgid   "network error"
msgstr  ""

#: /home/jtvi/Projects/Go/src/gitlab.com/jtvi/podcast/podcast_app/gtk_async_loader/error_view.go:40
msgid   "unknown error happened"
msgstr  ""

