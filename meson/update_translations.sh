#!/usr/bin/env bash

SOURCE_DIR=$1
PO_DIR=$2

go-xgettext -o "$PO_DIR/com.gitlab.jtvi.app.pot" --package-name=com.gitlab.jtvi.podcast \
 -k=translations.T $(echo $(find "$SOURCE_DIR" -type f -name '*.go' -printf '%p '))