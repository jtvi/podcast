#!/usr/bin/env bash

SOURCE_DIR="$1"
OUT_DIR="$2"
APP_ID="$3"
EXEC_NAME="$4"


(cd ${SOURCE_DIR} && go build -o ${APP_ID})
mv ${SOURCE_DIR}/${APP_ID} .
